---
title: Toelichting Ontologie
weight: 1
---
Voor KIK-V (het contextuele kader waarover de afspraken gaan) is een ontologie gespecificeerd. KIK-V beschrijft in deze ontologie de kennis over een onderwerp, met attributen en relaties naar andere onderwerpen. De ontologie is niet voor een specifieke toepassing beschreven, maar staat los van de toepassing. Meervoudig gebruik van data wordt hierdoor mogelijk.

De specificatie van de ontologie is vastgelegd in de [Web Ontology Language](https://www.w3.org/OWL/) (OWL). Deze standaard voorziet in hergebruik van specificaties door extensies en verwijzingen. Door middel van metadata wordt een contextueel kader beschreven, haar dataset, haar ontologie en de gegevensbronnen die geraadpleegd kunnen worden. Ieder onderwerp in de ontologie wordt gezien als raadpleegbare gegevensbron oftewel een resource.

![](toelichting_ontologie_1.png)

## Decompositie van het domein.

In een contextueel kader (hier: KIK-V) worden de gegevens vastgelegd in een dataset. De gegevens in de dataset worden vastgelegd overeenkomstig de ontologie die voor alle deelnemers binnen KIK-V is afgesproken. Het specificeren van een ontologie is een gezamenlijke verantwoordelijkheid van de bronhouders. De deelnemers die een contextueel kader in een afsprakenstelsel of toepassingsgebied delen.

De concepten worden beschreven in triples, bestaande uit een onderwerp, predicaat en object. Hiervoor wordt gebruik gemaakt van het [W3C Resource Description Framework](https://www.w3.org/RDF/). Voor de vastlegging van de concepten moet een Triplestore als database worden gebruikt.

![](toelichting_ontologie_2.png)

## Weergave van het concept cliënt (illustratief).

De triples worden linked data doordat in de objecten wordt verwezen naar andere gegevensbronnen. Hierdoor ontstaat een web van gegevens met relaties tussen gegevensbronnen. In bovenstaand voorbeeld is het zorgprofiel als linked data weergegeven.

De objecten hebben een unieke identificatie (persistent identifier/ URI) onafhankelijk van de bewaarlocatie. Het unieke label zorgt ervoor dat het object altijd teruggevonden kan worden, ook als de naam van het object of de bewaarplaats verandert.

Het conceptueel model beschrijft ook de structuur en betekenis van de relaties tussen de objecten. In de ontologie kunnen ook referenties opgenomen zijn naar objecten in een ander conceptueel kader of in een ander domein. Deze referenties worden opgenomen door middel van Linked data.