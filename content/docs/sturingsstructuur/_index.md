---
title: Sturingsstructuur
weight: 10
---
De eerste versie van de afsprakenset is ontwikkeld in een tijdelijk programma. De afspraken hebben echter een dynamisch karakter. Ontwikkelingen binnen en buiten KIK-V hebben invloed op de afspraken en moeten daarin een plek krijgen, nieuwe versies van de afsprakenset moeten worden vastgesteld en de afsprakenset moet duurzaam geborgd worden. Dit vraagt om een meer permanente sturingsstructuur voor de afspraken.

Deze sturingsstructuur ziet er in samenhang als volgt uit:

![Sturingsstructuur weergave](sturingsstructuur-weergave.png)

Hieronder wordt de sturingsstructuur op hoofdlijnen nader beschreven. De structuur is tevens verankerd in het [Convenant KIK-V](https://zoek.officielebekendmakingen.nl/stcrt-2021-13997.html). Voor een diepgaandere beschrijving van de verantwoordelijkheidsverdelingen tussen de verschillende gremia, zie [Gedetailleerde verantwoordelijkheidsverdeling sturingsstructuur](/content/docs/sturingsstructuur/verantwoordelijkheden.md).

## Functies en hun invulling
De sturingsstructuur geeft invulling aan verschillende functies:

* de **strategische-besluitvormingsfunctie**: dit betreft de (strategische) besluitvorming over het beheer, de doorontwikkeling, het gebruik en de naleving van de afsprakenset. Het gremium dat de strategische-besluitvormingsfunctie invult wordt ook wel Ketenraad KIK-V genoemd;
* de **tactische-besluitvormingsfunctie**: dit betreft de voorbereiding van de (strategische) besluitvorming en de besluitvorming over kleinere en urgente releases van de afsprakenset. Het gremium dat de tactische-besluitvormingsfunctie invult wordt ook wel het Tactisch overleg KIK-V genoemd;
* de **uitvoeringsfunctie**: dit betreft de uitvoering van de gezamenlijke beheerstaken rondom de afsprakenset, waarbij doorontwikkeling wordt gezien als onderdeel van beheer. Het gremium dat de uitvoeringsfunctie invult wordt ook wel beheerorganisatie genoemd;
* de **nalevingsfunctie**: dit betreft de vraag hoe aanbieders, afnemers en eventuele andere bij de sturingsstructuur betrokken partijen tot een zo volledig mogelijke naleving van de afspraken worden gebracht.
  Deze functies worden hieronder nader toegelicht en ingevuld.

### Strategische-besluitvormingsfunctie
* De Ketenraad KIK-V is verantwoordelijk voor (strategische) besluitvorming over het beheer, de doorontwikkeling, het gebruik en de naleving van de afsprakenset. Zij gaat daarmee onder meer over majeure releases van de afsprakenset en de doorontwikkelagenda (kaders en prioritering).
* De Ketenraad KIK-V bestaat ten minste uit (vertegenwoordigers van) aanbieders en (vertegenwoordigers van) afnemers op wie de afsprakenset KIK-V rechtstreeks betrekking heeft.

In praktijk gaat het dan om de aanbieders en afnemers die op dat moment of binnen niet al te lange tijd worden geraakt door afspraken in een of meerdere uitwisselprofielen.

* De samenstelling kan in de loop van de tijd wijzigen als er nieuwe (soorten) aanbieders of afnemers instappen, of bestaande deelnemers uittreden.
* (Vertegenwoordigers van) aanbieders en (vertegenwoordigers van) afnemers committeren zich aan hun deelname in de Ketenraad KIK-V middels het Convenant KIK-V. Deelnemers dienen onderling akkoord te gaan over de samenstelling van de Ketenraad KIK-V en de deelname van (nieuwe) partijen. Bij wijzigingen aan de samenstelling ondertekenen de deelnemers een nieuwe versie van het convenant of stellen ze een addendum op bij het convenant.
* Gezien het grote aantal aanbieders en afnemers waarop de afsprakenset betrekking heeft, kunnen zij niet allemaal individueel deelnemen aan de Ketenraad KIK-V. Vertegenwoordiging is als volgt geregeld:
  * Instellingen voor verpleeghuiszorg nemen deel aan de Ketenraad KIK-V via brancheorganisaties.
  * Indien zorgkantoren deelnemen aan de afsprakenset KIK-V, verloopt hun deelname aan de Ketenraad KIK-V via Zorgverzekeraars Nederland.
  * Indien enkelvoudige afnemers deelnemen aan de afsprakenset KIK-V, neemt elke afzonderlijke afnemer via een eigen vertegenwoordiger deel aan de Ketenraad KIK-V. Dit kan bijvoorbeeld betrekking hebben op de Inspectie Gezondheidszorg en Jeugd, de Nederlandse Zorgautoriteit, Zorginstituut Nederland (in haar rol rond de verplichte aanleveringen van verpleeghuizen op grond van artikel 66d van de Zorgverzekeringswet), het ministerie van VWS en Patiëntenfederatie Nederland.
* De Ketenraad KIK-V bestaat uit leden op bestuurlijk- of directieniveau, die een wederzijdse relatie (mandaat en gezag) hebben met hun achterban.
* De Ketenraad KIK-V kiest uit haar midden een voorzitter. Indien er onder deelnemers aan de Ketenraad KIK-V geen animo is voor het voorzitterschap, dan vult de beheerorganisatie deze rol in. De voorzitter wordt (her)benoemd voor een periode van één jaar.
* De beheerorganisatie ondersteunt de Ketenraad KIK-V en voert hiervoor het secretariaat.
* Besluitvorming vindt plaats op basis van consensus. Indien de deelnemers niet tot consensus weten te komen over een voorstel, dan wordt het voorstel niet aangenomen. Er is bewust geen escalatiemogelijkheid, omdat instemming van de partijen met de afspraken waar zij zichzelf of degenen die zij vertegenwoordigen aan willen binden een cruciaal kenmerk is van de afsprakenset KIK-V.
  Het functioneren van de Ketenraad KIK-V wordt minimaal eens in de twee jaar geëvalueerd en bij substantiële wijzigingen aan:
* De scope van de Afsprakenset (uitbreiding domeinen en type gegevens);
* De deelnemende partijen aan het convenant.

### Tactische-besluitvormingsfunctie

* Het Tactisch overleg KIK-V is verantwoordelijk voor de integrale voorbereiding van de (strategische) besluitvorming van de Ketenraad KIK-V.
* Door de invulling van de Ketenraad KIK-V bestaat de praktische noodzaak om enkele beslissingen via een lichtere structuur te kunnen nemen. Voor deze beslissingen geldt dat een integrale bestuurlijke afweging niet loont (bij mineure wijzigingen aan de afsprakenset) of een te lange doorlooptijd vergt (bij urgente wijzigingen aan de afsprakenset). Het Tactisch overleg KIK-V is bevoegd om te besluiten over deze mineure en urgente wijzigingen. Het Tactisch overleg KIK-V is daarnaast ook bevoegd om nieuwe en gewijzigde uitwisselprofielen vast te stellen, zolang deze passen binnen de kaders van de vastgestelde afsprakenset.
* De leden van de Ketenraad KIK-V wijzen elk een (beleids)medewerker aan als vertegenwoordiging in het Tactisch Overleg KIK-V. Deze (beleids)medewerkers zijn tevens dagelijks aanspreekpunt voor de uitvoering van de afspraken in het convenant en de afsprakenset (single point of contact).
* De beheerorganisatie ondersteunt het Tactisch Overleg KIK-V en voert hiervoor het secretariaat.
* Besluitvorming van het Tactisch Overleg KIK-V vindt plaats op basis van consensus. Indien de deelnemers niet tot consensus weten te komen over een voorstel, dan wordt het voorstel niet aangenomen. Indien een voorstel niet wordt aangenomen, past de beheerorganisatie het voorstel aan of escaleert het Tactisch Overleg KIK-V de besluitvorming naar de Ketenraad KIK-V.
* Het Tactisch Overleg KIK-V legt verantwoording af aan de Ketenraad KIK-V. Dit doet zij onder andere door de Ketenraad KIK-V te informeren over haar besluiten.
* Het functioneren van het Tactisch Overleg KIK-V wordt minimaal eens in de twee jaar geëvalueerd en bij substantiële wijzigingen aan:
  * De scope van de Afsprakenset (uitbreiding domeinen en type gegevens);
  * De deelnemende partijen aan het convenant.

### Uitvoeringsfunctie

#### Overwegingen en keuze beheerorganisatie

* Er zijn twee belangrijke overwegingen om voor de invulling van het beheer te kiezen voor een bestaande beheerorganisatie:
  * De taken voor het beheer van de afsprakenset zijn tamelijk specialistisch. Het is zeer de vraag of een beheerorganisatie die zich enkel richt op de afsprakenset genoeg volume heeft om de kwaliteit en continuïteit van de taakuitvoering te kunnen borgen;
  * Inpassing van het beheer van de afsprakenset in een bestaande uitvoeringsorganisatie kan tot synergie leiden, met name als die organisatie dichtbij het werkveld van KIK-V actief is, rond thema’s als langdurige zorg, informatiestandaarden en kwaliteitsstandaarden.
* Bij de inrichting van de beheerorganisatie is het van belang rekening te houden met deze overwegingen door bijvoorbeeld:
  * De taken zoveel mogelijk uit te laten voeren door medewerkers die actief zijn voor meerdere producten;
  * Medewerkers die betrokken zijn bij het beheer van de afsprakenset samen een (product)team te laten vormen, maar formeel onderdeel te laten zijn van een breder team voor het beheren van standaarden en/of afspraken;
  * De functionele sturing op het “team afsprakenset KIK-V” helder te beleggen, zodat sturing mogelijk is op de samenhang, kwaliteit en continuïteit van de taken rondom de afsprakenset. Dat kan bijvoorbeeld in de vorm van een “domeinmanager KIK-V”;
  * De uitvoering zo te organiseren dat er synergie is met de overige activiteiten van de beheerorganisatie, waaronder naast inhoudelijke activiteiten ook de bedrijfsvoeringsactiviteiten.
  * De beheerorganisatie wordt voor de eerste beheerfase ingevuld door Zorginstituut Nederland.
  * Zorginstituut Nederland heeft meerdere rollen in de sturingsstructuur: als beheerorganisatie en als (potentiële) afnemer. Vanwege de gewenste neutraliteit van de beheerorganisatie, zal de rol van afnemer door andere personen en organisatie-onderdelen binnen ZIN vervuld moeten worden. Het is aan ZIN om te bepalen hoe zij deze rol precies invult.

#### Aandachtspunten invulling beheerorganisatie

* De beheerorganisatie moet zodanig opereren dat zij wordt gezien als zeer deskundig en neutraal. Zij dient door alle partijen te worden vertrouwd als facilitator van het matchingsproces.
* De afsprakenset is een manier van partijen om tot wederzijdse afstemming te komen. Het matchingsproces is alleen effectief als het tot voorstellen leidt waar draagvlak voor is. Dit vraagt van de beheerorganisatie dat zij in haar handelen veel aandacht besteedt aan het verkrijgen van draagvlak voor specifieke voorstellen en voor de afsprakenset in zijn algemeenheid. Een efficiënte wijze om dat te doen is door de verschillende belanghebbende partijen met elkaar in gesprek te laten gaan. Van de beheerorganisatie wordt verwacht dat zij minimaal de volgende vormen van afstemming organiseert:
  * Integrale afstemming over plannen, op te leveren producten en resultaten. Denk aan afstemming over de strategische ontwikkelkalender, samenstelling van releases, voorstellen voor releases, etc. Deze afstemming verloopt via het Tactisch overleg KIK-V (zie tactisch-besluitvormingsfunctie);
  * Expertafstemming voor de ontwikkeling van (onderdelen van) producten: denk aan afstemming via referentiegroepen, met softwareleveranciers, met andere beheerorganisaties, met de Stuurgroep Kwaliteitskader Verpleeghuiszorg, met overige belanghebbende partijen, etc.

Voor het organiseren van deze afstemming/toetsing wordt vertrouwd op de professionaliteit en expertise van de beheerorganisatie. Er worden in de sturingsstructuur niet bij voorbaat verschillende aanvullende gremia voorgeschreven.

* De beheerorganisatie moet in haar handelen in goede verbinding staan met (derde) partijen die veel belang hebben bij of invloed hebben op de afsprakenset;
* De beheerorganisatie moet in haar handelen in goede verbinding staan met softwareleveranciers en met de andere partijen die kennis hebben van (gegevens in) het operationele proces. Dat draagt bij aan de technische kwaliteit van de afsprakenset en het maken van goede afwegingen over de inhoud ervan. Ook de kwaliteit van het gebruik is erbij gebaat;
* De beheerorganisatie is eerstverantwoordelijk voor de ‘technische’ kwaliteit van de afsprakenset. Denk daarbij aan eigenschappen als interne consistentie, versiebeheer en begrijpelijkheid van de producten. Zij kan dat doen vanuit haar neutrale en deskundige rol;
* De beheerorganisatie is gebonden aan de beheerafspraken en de kaders van de Ketenraad KIK-V en de opdrachtgever/financier. Rechten en plichten van de beheerorganisatie verkregen vanuit een andere rol, zijn niet van toepassing op de rol voor KIK-V. Invulling van de taak moet uiteraard niet op gespannen voet staan met andere verantwoordelijkheden van de beheerorganisatie;
* Het functioneren van de beheerorganisatie wordt minimaal eens in de twee jaar geëvalueerd;

#### Financiering en opdrachtgeverschap

* De beheerorganisatie wordt gefinancierd door het ministerie van VWS.
* Het ministerie van VWS treedt op als opdrachtgever van Zorginstituut Nederland.
* Het ministerie van VWS is als opdrachtgever en financier verantwoordelijk voor het vaststellen van de globale kaders, het budget en het bewaken van samenhang met andere initiatieven.
* De opdrachtgever/financier, Ketenraad KIK-V en beheerorganisatie stemmen gezamenlijk de beheeropdracht af.
* De opdrachtgever/financier geeft geen inhoudelijke sturing aan de (door)ontwikkeling van de afsprakenset en uitwisselprofielen. Deze verantwoordelijkheid is belegd bij de (vertegenwoordiging van) aanbieders en (vertegenwoordiging van) afnemers in de Ketenraad KIK-V. Het ministerie van VWS kan in de Ketenraad KIK-V als afnemer plaatsnemen.

Ook het Ministerie van VWS heeft meerdere rollen in de sturingsstructuur: die van opdrachtgever, financier en afnemer. Daarnaast kan VWS actief zijn op beleidsaspecten die de context beïnvloeden, denk bijvoorbeeld aan informatiebeleid. Het is aan VWS om te bepalen hoe zij de verschillende rollen naar en binnen KIK-V invult en hoe zij de interne afstemming tussen de verschillende directies borgt.

### Nalevingsfunctie

* Er wordt toezicht gehouden op de naleving van de afspraken over informatie-uitwisseling, zowel actief als reactief. Een voorbeeld van actief is een door de partijen vooraf afgesproken onderzoek over de naleving van de uitwisselprofielen en een voorbeeld van reactief is optreden naar aanleiding van een melding.
* Bij niet-naleving kunnen partijen elkaar, al dan niet via vertegenwoordigers, aanspreken via de Ketenraad KIK-V.
* Na de eerste periode van operatie wordt bezien of er naast bestuurlijk aanspreken behoefte is aan een juridisch handhavingsinstrumentarium.
* De afspraken over het onderhoud, de doorontwikkeling en het toezicht op de naleving van de afsprakenset KIK-V zijn vastgelegd in beheerafspraken. Deze beheerafspraken worden tevens geëxpliciteerd in het convenant en bekrachtigd door deelnemers van de Ketenraad KIK-V en de beheerorganisatie.