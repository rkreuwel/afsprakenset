---
title: Privacy en informatiebeveiliging deelnemers
weight: 5
---
Afnemers en aanbieders zijn zelf verantwoordelijk voor een gedegen privacy- en informatiebeveiligingsbeleid en dienen voor de eigen organisatie de noodzakelijke maatregelen te treffen om privacy en informatiebeveiliging te borgen. De volgende handleidingen kunnen daarbij handvatten bieden:

* [Handleiding Algemene verordening gegevensbescherming (AVG)](https://www.rijksoverheid.nl/documenten/rapporten/2018/01/22/handleiding-algemene-verordening-gegevensbescherming). Uitgegeven door het Ministerie van Justitie en Veiligheid.
* [Guidelines on Data Protection Impact Assessment (DPIA) (wp248rev.01)](https://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=611236). Van de voorganger van de EDPB, waarvan ook een Nederlandstalige versie beschikbaar is.

Daarnaast toetst de beheerorganisatie doorlopend de privacy en informatiebeveiliging van de uitwisselprofielen en de bredere afsprakenset (zie hiervoor [Toetsing privacy- en informatiebeveiliging](/content/docs/beheer_en_onderhoud/privacy_en_informatiebeveiliging.md)).

## Logging
Van afnemers en aanbieders wordt verwacht dat zij verwerkingen in het kader van KIK-V loggen. Zo kan tijdig worden ingegrepen bij onrechtmatige verwerkingen, hetzij als gevolg van (on)opzettelijk foutief handelen, kwaadwillende derden of beheersfouten.

Voor aanbieders volgt de grondslag om te loggen uit een combinatie van de Algemene Verordening Gegevensbescherming, de Wet aanvullende bepalingen verwerking persoonsgegevens in de zorg en het Besluit elektronische gegevensverwerking door zorgaanbieders (zie [Juridisch kader](/content/docs/juridisch-kader/)). In deze wet- en regelgeving is tevens vastgelegd dat logging moet plaatsvinden volgens de NEN-7513 norm.

Voor afnemers bestaan verschillende grondslagen voor logging, afhankelijk per afnemer en situatie. De afnemer dient in het kader van de eigen bedrijfsvoering te bepalen op welke wijze logging wordt toegepast, wat hiervoor de grondslag is en in hoeverre daarbij persoonsgegevens van medewerkers worden verwerkt (zie [Juridisch kader](/content/docs/juridisch-kader/)).

## Verwerking persoonsgegevens medewerkers
Bij het aggregeren van gegevens voor verstrekking binnen KIK-V (door aanbieders) en bij de randvoorwaardelijke logging (door aanbieders en afnemers) worden persoonsgegevens van medewerkers verwerkt. Aanbieders en afnemers zijn zelf verantwoordelijkheid om deze verwerkingen, waarin persoonsgegevens van het personeel worden verwerkt, ter goedkeuring voor te leggen bij de ondernemingsraad van de eigen organisatie (indien aanwezig). Dit voor zover de afnemer/aanbieder dit niet al heeft gedaan in het kader van het algemene informatiebeveiligingsbeleid. De verplichting volgt uit artikel 27 lid 1 sub k van de Wet op de ondernemingsraden (zie [Juridisch kader](/content/docs/juridisch-kader/)).

## Aanvullende aanbevolen maatregelen
Aanbieders en afnemers wordt aanbevolen om gegevens verzameld in het kader van KIK-V in rust te versleutelen. Zo kunnen ongeautoriseerde toegang en wijzigingen van gegevens worden voorkomen.

In aanvulling daarop wordt aanbieders aanbevolen om 1) periodiek te toetsen of de aggregatie van persoonsgegevens daadwerkelijk leidt tot onomkeerbare anonimisering en 2) een infrastructuur te gebruiken die binnen de Europese Economische ruimte valt bij het verwerken van persoonsgegevens voor de aggregatie.
