---
title: Uitwisselkalender
weight: 8
---
De uitwisselkalender beschrijft in samenhang de aanlever- en terugkoppelmomenten binnen KIK-V en is daarmee een instrument dat inzicht geeft in de timing van de gegevensuitwisseling. Ook de peilmomenten of verslagperiode van de gegevens spelen daarbij een rol. Dit inzicht is relevant voor zowel aanbieders (wanneer moeten bepaalde gegevens klaarstaan?) als afnemers (wanneer zijn bepaalde gegevens beschikbaar?).

Voor nieuwe informatiebehoeften van ketenpartijen in de afsprakenset functioneert de kalender als referentiekader. Er vindt sturing plaats op het aansluiten bij een bestaand moment van uitvraag en peilmomenten of verslagperiode. Na vaststelling van een (wijziging op een) uitwisselprofiel wordt het aanlever- en/of peilmoment toegevoegd aan de uitwisselkalender.

In een tijdlijn ziet dit er als volgt uit:

![Uitwisselkalender_2](uitwisselkalender_2.PNG)

De uitwisselprofielen kennen de volgende overlap aan concepten:

![Uitwisselkalender_1](uitwisselkalender_1.PNG)

In de bovenstaande figuur is de overlap op conceptniveau in kaart gebracht. De concepten zijn onderdeel van de modelgegevensset en kunnen eigenschappen hebben en onderling aan elkaar gerelateerd zijn. De eigenschappen van en relaties tussen concepten worden hierin niet weergegeven. Met behulp van deze concepten kan antwoord op de indicatoren gegeven worden. 

Voorbeeld: een afnemer vraagt om het aantal zorgverleners dat op x datum in dienst is bij een zorgaanbieder (indicator). Om dit te kunnen berekenen, is het van belang om te weten welke medewerkers zorgverlener zijn (o.b.v. functie). Hiervoor is het concept **Zorgverlener (functie)** nodig. Vervolgens wordt gekeken welke zorgverleners een arbeidsovereenkomst hebben. Hiervoor is het concept **Arbeidsovereenkomst** nodig. Tot slot wordt gekeken naar het aantal zorgverleners met een arbeidsovereenkomst op een bepaalde datum. Hiervoor is het van belang om te weten wat de start- en einddatum van een arbeidsovereenkomst zijn, om te bepalen welke personeelsleden meegeteld moeten worden. De start- en einddatum zijn eigenschappen van een arbeidsovereenkomst en worden daarom niet als aparte concepten opgenomen.