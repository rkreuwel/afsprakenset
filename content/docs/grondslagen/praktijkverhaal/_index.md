---
title: Praktijkverhaal
weight: 4
---
Het praktijkverhaal beschrijft op toegankelijke wijze de praktische toepassing en toegevoegde waarde van de afsprakenset voor de verschillende belanghebbenden ([Het zorgkantoor](/content/docs/grondslagen/praktijkverhaal/het_zorgkantoor.md), [Inspecteurs](/content/docs/grondslagen/praktijkverhaal/inspecteurs.md) & [Zorgverleners](/content/docs/grondslagen/praktijkverhaal/zorgverleners.md))