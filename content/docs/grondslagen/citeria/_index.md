---
title: Criteria
weight: 2
---
De sectie Criteria bestaat uit:

* [Doelen](/content/docs/grondslagen/citeria/doelen.md)
* [Randvoorwaarden](/content/docs/grondslagen/citeria/randvoorwaarden.md)