---
title: Afspraken gegevensuitwisseling datastations
weight: 9
---
De wijze waarop afnemers en aanbieders het KIK-V vraag- en antwoordspel vormgeven, is (mede) afhankelijk van de mate van digitalisering van de betrokken actoren. De afspraken in dit hoofdstuk beschrijven het vraag- en antwoordspel met gebruik van datastations. Afnemers en aanbieders die een datastation willen implementeren voor toepassing binnen KIK-V, kunnen zich met deze afspraken voorbereiden.

## Status van de afspraken

Met de afspraken in dit hoofdstuk kunnen afnemers en aanbieders zich oriënteren op de implementatie van een datastation en de gegevensuitwisseling daarmee voor toepassing binnen KIK-V. Gezien het innovatieve karakter van het decentrale model van gegevensuitwisseling, vergen een aantal zaken de komende tijd nog nadere uitwerking (bijv. of, hoe en op welke termijn KIK-V overstijgende authentieke bronnen kunnen worden ontsloten). Het programma heeft afstemming met de relevante actoren om de resterende zaken nader uit te werken. Partijen kunnen vooralsnog geen rechten ontlenen aan de afspraken.

De afspraken over de gegevensuitwisseling met datastations zijn als volgt opgebouwd:

1. Functionele beschrijving vraag- en antwoordspel
2. Gehanteerde principes
3. Benodigde verifieerbare informatie voor een decentraal KIK-V proces
4. Technische implementatie
5. Beheerafspraken gegevensuitwisseling datastations

De eerste twee onderwerpen zijn onderdeel van de inleiding op deze pagina. Op een onderliggende pagina is punt 3 kort toegelicht. De overige twee onderwerpen worden in overleg met betrokkenen de komende tijd verder uitgewerkt.

## Functionele beschrijving vraag- en antwoordspel

De technische afspraken over gegevensuitwisseling met datastations ondersteunen het volgende vraag- en antwoordspel:

1. Afnemer bepaalt (op basis van gehele populatie aanbieders) aan welke aanbieders zij een gevalideerde vraag wilt stellen;
2. Afnemer bepaalt (per individuele aanbieder) hoe een aanbieder (technisch) te benaderen;
3. Afnemer stelt de gevalideerde vraag aan aanbieder;
4. Aanbieder ontvangt de gevalideerde vraag en controleert of de afnemer de vraag mag stellen;
5. Aanbieder stelt het antwoord (geautomatiseerd) samen, controleert het antwoord en geeft het antwoord vrij;
6. Aanbieder stuurt het antwoord retour naar afnemer;
7. Afnemer ontvangt het antwoord en bevestigt ontvangst aan aanbieder.

![Functionele beschrijving vraag en antwoordspel](functionele_beschrijving_vraag_en_antwoordspel.png)

Hierboven en op de onderliggende pagina’s wordt beschreven hoe dit functionele vraag- en antwoordspel zich vertaalt in een technische uitwerking.

## Gehanteerde principes

Voor de gegevensuitwisseling met datastations zijn de algemene principes van de Afsprakenset KIK-V van toepassing. Hieruit volgen zaken zoals de toepassing van de Principes Informatiestelsel voor de zorg (P12) en de FAIR-dataprincipes (P10).

Daarbovenop zijn de afspraken opgebouwd aan de hand van de volgende drie principes:

1. Schaalbaar decentraal model;
2. Zorginfrastructuur-onafhankelijk;
3. Correcte implementatie is eigen verantwoordelijkheid afnemer/aanbieder.

Deze principes worden hieronder nader toegelicht.

### Principe 1. Schaalbaar decentraal model

De potentie van de KIK-V werkwijze voor het meervoudig gebruik van data is groot. Doordat gegevens, inclusief de onderlinge samenhang tussen/betekenis van deze gegevens, gestandaardiseerd in datastations van aanbieders worden vastgelegd, is het mogelijk om met dezelfde gegevens te voorzien in antwoorden op een groot palet aan vragen.

De technische uitwisseling met datastations dient te voorzien in dezelfde potentie aan schaalbaarheid. Om die reden is ervoor gekozen om het vraag- en antwoordspel zoveel mogelijk decentraal in te richten. Dit kent de volgende kenmerken:

* Het vraag- en antwoordspel vindt rechtstreeks plaats tussen afnemer en aanbieder, zonder directe tussenkomst van een derde partij. Dit maakt de uitvoering van het vraag- en antwoordspel onafhankelijk van condities (beschikbaarheid, etc.) van derde partijen en voorzieningen;
* Voor informatie in dit rechtstreekse proces waarbij de afnemer en aanbieder zelf niet de bron/autoriteit van de informatie vormen, wordt teruggevallen op verifieerbare cryptografische bewijzen van autoriteiten (denk aan de wijze waarop bij de douane wordt teruggevallen op een identiteitsbewijs).

Voor het genereren van verifieerbare cryptografische bewijzen wordt de W3C-standaard Verifiable Credentials toegepast. Deze standaard kent drie rollen, namelijk die van _issuer_ (uitgever), _holder_ (houder) en _verifier_(verificateur). De _issuer_ is een derde partij die autoriteit is over bepaalde informatie. Deze geeft een verklaring uit in de vorm van een verifieerbaar cryptografisch bewijs. De _holder_ ontvangt dit verifieerbare bewijs en is de partij waarover de verklaring gaat. De _holder_ past het technische bewijs toe in het rechtstreekse, decentrale proces met de _verifier_. Om de informatie in het bewijs te kunnen vertrouwen, controleert de _verifier_ de waarachtigheid van de verklaring van de _issuer_. Dit doet de _verifier_ tegen een onderliggende gezamenlijke vertrouwensregistratie (verifiable data registry). _Issuer_, _holder_ en _verifier_ zijn allen technisch aangesloten op deze _verifiable data registry_.

Zie voor de onderlinge verhoudingen tussen de rollen ook onderstaande afbeelding.

![Principe 1](principe_1.png)

_Figuur 1. W3C-standaard Verifiable Credentials_

De komende tijd wordt verder uitgewerkt welke informatie middels Verifiable Credentials beschikbaar moet zijn voor afnemers en aanbieders ten behoeve van het decentrale KIK-V proces.

### Principe 2. Zorginfrastructuur-onafhankelijk

KIK-V realiseert en beheert geen zorginfrastructuur en maakt hergebruik van veldafspraken hierover. Het KIK-V vraag- en antwoordspel via datastations en met gebruik van de standaard Verifiable Credentials moet in principe toe te passen zijn via elke onderliggende zorginfrastructuur.

**Voorlopige afbakening zorginfrastructuren**

Het streven om zorginfrastructuur-onafhankelijk te werken, levert op korte termijn een aantal praktische issues op. In het geval dat (individuele) afnemers en aanbieders afwijkende keuzes maken voor de infrastructuur waarop zij hun koppelvlak baseren, dan komt dit samen als een interoperabiliteitsissue binnen KIK-V. Koppelvlakken uit verschillende infrastructuren begrijpen elkaar namelijk niet zondermeer.

Op dit moment zijn er (nog) geen landelijke afspraken om dit interoperabiliteitsissue op te vangen. Door de KIK-V ketenpartijen is daarom aan het programma gevraagd om voorlopig de **toepassing van Nuts te verkennen**. Omdat zorgkantoren binnen het programma iWLZ een implementatie met nID beproeven, is **daarnaast een traject opgestart om interoperabiliteit tussen Nuts en nID te realiseren**. Dit traject wordt begeleid vanuit het programma iWLZ en het programma KIK-V is hierbij aangehaakt om toe te zien op een werkbare oplossing voor de ketenpartijen van KIK-V. Uitkomsten worden, waar relevant voor de Afsprakenset KIK-V, opgenomen in een toekomstige versie van de afsprakenset.

### Principe 3. Correcte implementatie is eigen verantwoordelijkheid afnemer/aanbieder

KIK-V bouwt, net als andere use cases voor gegevensuitwisseling in de zorg, zoveel mogelijk voort op (landelijke) afspraken op het gebied van zorginfrastructuur en informatiebeveiliging. De overtuiging vanuit KIK-V is dat het steeds opnieuw vormgeven van toetsing op deze afspraken binnen de verschillende initiatieven niet de meest schaalbare oplossing in Nederland vormt. KIK-V voorziet daarom niet in aparte toetsing van correcte implementatie van KIK-V overstijgende afspraken (NEN7510/7512/7513, koppelvlakken van zorginfrastructuren, etc.). Waar daar een bredere behoefte voor is, zou toetsing van een correcte implementatie van deze bredere afspraken KIK-V overstijgend kunnen worden vormgegeven. Voor zover het de implementatie van KIK-V betreft, wordt de correcte implementatie van deze afspraken als de eigen verantwoordelijkheid van afnemers en aanbieders gezien.

Voor KIK-V specifieke zaken, zoals de correcte implementatie van het KIK-V vraag en antwoordspel, worden afnemers en aanbieders in staat gesteld de implementatie zelfstandig te toetsen. Hiervoor wordt een “roze vink” ontwikkeld. Dit is een toepassing waartegen afnemers en aanbieders de werking van het koppelvlak van hun datastation kunnen toetsen. Omwille van de schaalbaarheid ligt de verantwoordelijkheid voor de toetsing wel bij afnemers en aanbieders zelf. Er is bewust voor gekozen om deze toetsing niet centraal vorm te geven vanuit de Beheerorganisatie KIK-V.

De beheerorganisatie KIK-V voorziet overigens wel in een meldpunt voor problemen bij de gegevensuitwisseling. Dit meldpunt heeft met name als functie om patronen in meldingen te identificeren en deze neer te leggen bij de relevante probleemeigenaar.