---
title: Procedure afwijkende uitvraag
weight: 1
---
De procedure afwijkende uitvraag bevat de volgende afspraken:

- De afnemer informeert (de leden van) de Ketenraad KIK-V schriftelijk over de situatie en onderbouwt daarbij het voorgenomen besluit om (deels) af te wijken van de KIK-V procedures. 
- De afnemer hanteert in het voorstel voor de afwijking zoveel mogelijk de KIK-V principes zoals beschreven in de grondslagen van de afsprakenset. De afnemer wint hierover advies in bij de beheerorganisatie.
- De afnemer voert een privacytoets uit op het voorstel. In deze toets is aandacht voor zaken als rechtmatigheid van de uitvraag, komen tot dataminimalisatie evenals impact voor de administratie bij zorgaanbieders en eventuele maatregelen om te voorkomen dat antwoorden niet voldoende geanonimiseerd blijven (mogelijkheden tot (her)identificatie van personen, bijvoorbeeld door stapeling van gegevens). Dit laatste geldt vooral in de situatie dat er geen grondslag is voor het verwerken van persoonsgegevens door de afnemer, maar past ook bij de principes van KIK-V. Indien de uitvraag meer structureel van aard wordt, dan past de uitvraag bij eventuele uitwerking naar een uitwisselprofiel al binnen de principes.
- De leden van de Ketenraad KIK-V krijgen ten minste anderhalve week de gelegenheid om schriftelijk te reageren (in de zomermaanden juli en augustus minimaal drie weken) en mogen verzoeken om een (spoed)vergadering om het voorgenomen besluit te bespreken. Bovendien kan de beheerorganisatie gevraagd worden om advies.
Het uiteindelijke eindoordeel over het al dan niet voortzetten van de ingezette koers blijft aan de afnemer. Enkel de afnemer kan bepalen of zij met een alternatieve oplossing kan voldoen aan haar verplichtingen.
- De afnemer maakt, indien de ingezette koers wordt voortgezet, afspraken met de Ketenraad KIK-V over de scope en duur van de afwijking en wanneer de afwijking wordt geëvalueerd. 
- Situaties waarin de procedure afwijkende uitvraag wordt gevolgd, worden altijd geëvalueerd, opdat maximaal kan worden geleerd (ook met betrekking tot de procedure zelf) en dit soort situaties in de toekomst zoveel mogelijk kunnen worden voorkomen.
- Mocht blijken dat er sprake is van een repeterende informatiebehoefte, dan wordt zo snel mogelijk weer aangesloten bij de reguliere procedures. In de Ketenraad KIK-V worden hierover afspraken gemaakt en opgenomen op de ontwikkelagenda. De bedoeling is om zo snel als mogelijk die informatiebehoefte conform de afspraken KIK-V te laten verlopen.