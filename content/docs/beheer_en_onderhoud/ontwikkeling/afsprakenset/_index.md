---
title: Ontwikkeling Afsprakenset
weight: 1
---
## Wijzigingsproces

- De beheerorganisatie onderhoudt de afsprakenset en uitwisselprofielen en richt hiervoor een wijzigingsproces in.
- De afnemer maakt een nieuwe of gewijzigde informatiebehoefte kenbaar bij de beheerorganisatie. Ook andere deelnemers of de beheerorganisatie kan pro-actief een benodigde of gewenste wijziging voorstellen (bijvoorbeeld vanuit ontwikkelingen in de omgeving). 
- De beheerorganisatie ziet toe op de logische samenhang van uitwisselprofielen, evenals tussen uitwisselprofielen en de afsprakenset en bepaalt samen met de deelnemer of een (gewijzigde) informatiebehoefte een nieuw uitwisselprofiel behoeft of kan worden toegevoegd aan een bestaande, plus benodigde wijzigingen of aanvullingen op de afsprakenset.
- Deelnemers en de beheerorganisatie kunnen wijzigingsvoorstellen indienen. Een volledig uitgewerkt wijzigingsverzoek bevat:
  - Een beschrijving van het ervaren probleem c.q. de behoefte, inclusief de situatie(s) waarin deze wordt ervaren.
  - Een analyse van de grondoorzaken.
  - De voorgestelde wijzigingen aan de afsprakenset en/of uitwisselprofiel(en) en de impact op de rest van de afspraken.
  - Eventuele alternatieve oplossingen.
  - Een beschrijving van de implicaties voor de implementatie voor de belanghebbende partijen.
  - Een beschrijving van de wijze van implementatie die de minste impact en verstoringen veroorzaakt.
  - Een beschrijving van het draagvlak voor de voorgestelde wijziging.
  - Indien niet evident, een beschrijving van de business case.
- De beheerorganisatie doorloopt bij een wijzigingsverzoek aan de modelgegevensset eerst het [Matchingsproces](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces) en hanteert vervolgens bij de onderbouwing van een eventueel wijzigingsverzoek het bij dit proces beschreven Afwegingskader uitbreiding modelgegevensset.
- De beheerorganisatie is samen met de afnemer verantwoordelijk voor het vertalen van de informatiebehoefte naar een uitwisselprofiel. Middels het [Matchingsproces](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces) wordt op gestructureerde wijze de informatiebehoefte vertaald naar binnen KIK-V toepasbare informatievragen, concepten, definities en gegevenselementen. De beheerorganisatie ondersteunt in dit proces de afnemer waar nodig bij het (her)formuleren van zijn informatiebehoefte.
- Een wijzigingsverzoek hoeft bij het indienen niet compleet te zijn. De beheerorganisatie ziet toe op de kwaliteit en volledigheid van ingediende wijzigingsvoorstellen door:
  - Indieners van wijzigingen te adviseren en begeleiden;
  - De relatie met de strategische kaders, releasekalender en andere wijzigingsverzoeken te borgen;
  - Te borgen dat voorgestelde wijzigingen voldoen aan vigerende wet- en regelgeving en aansluiten bij de principes van KIK-V;
  - Fora (referentiegroepen) te organiseren waarin tot inhoudelijke afstemming en draagvlak kan worden gekomen. In deze fora worden naast deelnemers, onder andere ook softwareleveranciers betrokken; 
  - Te borgen dat eventueel auteursrecht op een voorgestelde oplossing wordt overgedragen;
  - Eindredactie te voeren over wijzigingen en deze te voorzien van een (onafhankelijk) advies.
- De beheerorganisatie faciliteert de benodigde afstemming en toetsing bij belanghebbende partijen.
- De beheerorganisatie archiveert wijzigingsvoorstellen.

## Besluitvorming nieuwe release
- De beheerorganisatie legt de wijzigingsverzoeken gebundeld voor ter besluitvorming.
- De beheerorganisatie onderzoekt, alvorens wijzigingsverzoeken ter besluitvorming worden voorgelegd, de impact op bestaande uitwisselprofielen. De uitkomst wordt meegenomen in de besluitvorming.
- De beheerorganisatie faciliteert de besluitvorming en de daarvoor benodigde informatievoorziening.
- De Ketenraad KIK-V besluit over het al dan niet doorvoeren van (majeure) wijzigingen en ziet toe op de samenhang met de uitwisselprofielen. Het Tactisch Overleg KIK-V besluit over kleine en/of spoedwijzigingen en legt hierover verantwoording af aan de Ketenraad KIK-V.

## Publicatie, evaluatie, implementatie
- De beheerorganisatie is na vaststelling verantwoordelijk voor het verwerken van de wijzigingsverzoeken in een nieuwe publicatie van de afsprakenset en/of uitwisselprofiel(en).
- De beheerorganisatie publiceert de afsprakenset en/of uitwisselprofiel(en) op een voor aanbieders en afnemers toegankelijke plek. 
- De beheerorganisatie voorziet in de benodigde informatievoorziening richting deelnemers ten behoeve van een soepele implementatie.
- De beheerorganisatie evalueert in samenspraak met de deelnemers zowel het proces voor de totstandkoming als de inhoud van de nieuwe release. 
- De beheerorganisatie kan, waar nodig en gepast, totstandkoming en onderhoud van voorzieningen faciliteren die een rol spelen bij de gegevensuitwisseling zoals beschreven in de afsprakenset.

De ontwikkeling van wijzigingsverzoeken, bundeling tot releases en vaststelling van deze releases door het Tactisch Overleg en de Ketenraad ziet er in samenhang als volgt uit:

![Wijzigings-proces](wijzigings-proces.png)

## Releasemanagement

- Geplande doorontwikkeling van de afsprakenset vindt zoveel mogelijk plaats volgens een vaste releasecyclus en -kalender. 
- Doorontwikkeling van een uitwisselprofiel vindt plaats aan de hand van een roadmap van de Ketenraad in een agile ontwikkelproces waarbij het Tactisch Overleg de rol heeft om met de beheerorganisatie de realisatie en releases van een uitwisselprofiel te plannen. 
- De volgende strategische overwegingen spelen minimaal een rol bij de totstandkoming van de strategische releasekalender:
  - Welke aan KIK-V gerelateerde vraagstukken behoeven aandacht en een gemeenschappelijke inspanning van afnemers en aanbieders? Denk aan:
    - Het uitbreiden van de beschikbare gegevens met andere thema’s;
    - Het verbeteren van de kwaliteit van gegevens;
    - Het verduidelijken van de context van gegevens;
    - Het verbeteren van de manier van gegevensuitwisseling;
    - Het uitbreiden naar andere domeinen in de zorg;
    - Het uitbreiden van de toepassingsgebieden van KIK-V;
    - De andere in praktijk ervaren knelpunten.
    - Welke van deze vraagstukken passen binnen de scope van KIK-V?
    - Welke van deze vraagstukken creëren op korte termijn de meeste meerwaarde?
    - Welke van deze vraagstukken zijn op lange termijn voor KIK-V van belang en dienen al tijdig te worden opgepakt om op tijd een oplossing te kunnen bieden?
    - Welke van deze vraagstukken brengen de meeste kans op succes met zich mee gezien de potentiële oplossingsruimte? Denk aan:
      - Politieke aandacht voor het vraagstuk;
      - Beschikbare financiële middelen;
      - Initiatief bij de betrokken partijen;
      - Belangen van invloedrijke stakeholders.
- De beheerorganisatie faciliteert de totstandkoming en vaststelling van een strategische releasekalender (meerjarig en jaarlijks) voor de doorontwikkeling. De releasekalender bevat de belangrijkste mijlpalen en voorgenomen wijzigingen per geplande release van de afsprakenset.
- De afsprakenset kent jaarlijks maximaal twee releases. De Ketenraad stelt deze vast.
- Daarnaast zijn er zes placeholder releases voor verzamelde wijzigingen op de afsprakenset en uitwisselprofielen. Op basis van een impactanalyse wordt bepaald of deze wijzigingen in een tussentijdse release kunnen worden opgenomen. Wijzigingen die hierin meegaan zijn bugfixes die een verstorende werking hebben op implementaties, omdat het issues betreft die het correct functioneren van  indicatoren verhinderen. Het voorstel voor de placeholder release bevat ook een voorstel voor de termijn waarbinnen implementatie plaats moet vinden. Het Tactisch Overleg wordt gevraagd om een schriftelijke bevestiging.
- Indien een impactanalyse met betrekking tot een wijziging leidt tot de conclusie dat directe oplossing nodig is dan leidt dit tot een hotfix. Het gaat hier om issues die een probleem veroorzaken voor privacy en beveiliging van de gegevensuitwisseling. Ook typefouten worden op deze manier verholpen. De beheerorganisatie voert deze wijzigingen door en informeert hierover het Tactisch Overleg.   
- De Ketenraad KIK-V besluit over de vaststelling van de strategische releasekalender. Een vertegenwoordiging van deelnemers wordt minimaal geconsulteerd in het besluitvormingsproces.
- Wijzigingsvoorstellen dienen te passen binnen de vastgestelde strategische releasekalender.
- Er mag worden afgeweken van de releasekalender bij:
  - Wijzigingen die nodig zijn om een onmiddellijke dreiging voor de continuïteit van of het vertrouwen in de gegevensuitwisseling binnen KIK-V af te wenden;
  - Verbeteringen waarvan de baten van spoedig doorvoeren significant groter zijn dan de implementatie-inspanningen, en die op breed draagvlak onder de Deelnemers kunnen rekenen.

## Relaties met aanpalende ontwikkelingen en standaarden buiten KIK-V

- De beheerorganisatie ziet actief toe op de relaties met aanpalende ontwikkelingen (bijvoorbeeld veranderende wetgeving) en standaarden buiten KIK-V (zoals ZIB’s), behartigt hierbij de belangen van KIK-V en signaleert relevante wijzigingen. 
- De beheerorganisatie bepaalt de impact van aanpalende ontwikkelingen en standaarden buiten KIK-V op de eigen afsprakenset en/of uitwisselprofiel(en) en komt waar nodig met wijzigingsvoorstellen.
- De beheerorganisatie is verantwoordelijk om het gebruik van de externe standaard te evalueren, zowel procesmatig (bijvoorbeeld de manier waarop externe standaard met wijzigingen omgaat) als inhoudelijk (in hoeverre sluit de standaard nog voldoende aan).