---
title: Openbaar Databestand ZIN
weight: 5
---
Het Zorginstituut draagt zorg voor het verzamelen, samenvoegen en beschikbaar maken van informatie over de kwaliteit van verleende zorg. Deze informatie die het Zorginstituut via de [Openbare Database](https://www.zorginzicht.nl/openbare-data) ontsluit, wordt gebruikt als keuze-informatie door cliënten, bij de zorginkoop door zorgverzekeraars en in de handhaving door de Inspectie voor de Gezondheidszorg.

In de Openbare Database bij het Zorginstituut Nederland staat een grote hoeveelheid gegevens over de kwaliteit van zorg. De informatie uit de Openbare Database is voor iedereen toegankelijk. De bestanden zijn bedoeld voor de professionele gebruiker van kwaliteitsgegevens; onderzoekers, zorgverzekeraars en zorgaanbieders maken er gebruik van. De informatie uit de database is voor de consument niet direct inzichtelijk en bruikbaar; pas na het gebruiksklaar maken voor presentatie aan zorgconsumenten worden de gegevens via websites als ZorgkaartNederland aangeboden. De database is te raadplegen via de website [Zorginzicht](https://www.zorginzicht.nl/openbare-data); [https://www.zorginzicht.nl/openbare-data/open-data-verpleeghuiszorg](https://www.zorginzicht.nl/openbare-data/open-data-verpleeghuiszorg).

De ODB bevat kwaliteitsgegevens. Op de Transparantiekalender staan de afspraken die het Zorginstituut met zorgaanbieders maakt over het aanleveren van kwaliteitsgegevens. Op de deze lijst staat met welke meetinstrumenten, kwaliteitsindicatoren en vragenlijsten, de kwaliteit van de verleende zorg wordt gemeten en wanneer die meetgegevens worden aangeleverd bij Zorginstituut Nederland. Van deze in het Register opgenomen meetinstrumenten hebben zorgaanbieders aangegeven dat de uitkomsten bedoeld zijn voor keuze-informatie voor burgers en zorginkoop door zorgverzekeraars. Zorgaanbieders zijn wettelijk verplicht informatie over de kwaliteit van verleende zorg conform afspraken in de Transparantiekalender aan te leveren bij het Zorginstituut. Zie voor de Transparantiekalender: [https://www.zorginzicht.nl/transparantiekalender](https://www.zorginzicht.nl/transparantiekalender).

Meer informatie over de gegevens die worden ontsloten via de ODB:

* [https://www.zorginzicht.nl/kwaliteitsinstrumenten/verpleeghuiszorg-kwaliteitskader-indicatoren](https://www.zorginzicht.nl/kwaliteitsinstrumenten/verpleeghuiszorg-kwaliteitskader-indicatoren)
* [https://www.zorginzicht.nl/kwaliteitsinstrumenten/verpleeghuiszorg-personeelssamenstelling-indicatoren](https://www.zorginzicht.nl/kwaliteitsinstrumenten/verpleeghuiszorg-personeelssamenstelling-indicatoren)

Het Kwaliteitsverslag is onderdeel van de set indicatoren die wordt uitgevraagd via het Zorginstituut. Deze is om die reden niet als aparte Openbare Bron genoemd maar onderdeel van de Openbare Database van het Zorginstituut.