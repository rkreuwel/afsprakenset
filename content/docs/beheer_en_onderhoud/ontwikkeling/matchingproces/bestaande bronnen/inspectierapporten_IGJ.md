---
title: Inspectierapporten IGJ
weight: 4
---
De Inspectie Gezondheidszorg en Jeugd (IGJ) controleert onder meer de veiligheid en kwaliteit van de zorg in verpleeghuizen. Zorgverleners en zorgaanbieders hebben samen afgesproken waar goede zorg aan moet voldoen. Als blijkt dat de veiligheid en kwaliteit van de zorg onvoldoende is, nemen we maatregelen.

Zij doet dit bijvoorbeeld door het doen van inspectiebezoeken.

Tijdens deze inspectiebezoeken gebruiken zij het [toetsingskader voor de gehandicaptenzorg en de verpleeghuiszorg](https://www.igj.nl/zorgsectoren/gehandicaptenzorg/publicaties/toetsingskaders/2017/07/01/toetsingskader-voor-zorgaanbieders-waar-mensen-wonen-die-langdurige-zorg-nodig-hebben). In het toetsingskader staat welke informatie wordt opgehaald tijdens een inspectiebezoek

Tijdens een inspectiebezoek kunnen de volgende onderwerpen aan bod komen:

* Is de zorg persoonsgericht?
* Is de zorgverlener deskundig? En worden zorgverleners goed ingezet?
* Biedt de zorgaanbieder een veilig werkklimaat?
* Stuurt de zorgaanbieder goed op kwaliteit en veiligheid?
* Cultuur in de organisatie

Zie voor meer informatie: [https://www.igj.nl/zorgsectoren/gehandicaptenzorg/publicaties/toetsingskaders/2017/07/01/toetsingskader-voor-zorgaanbieders-waar-mensen-wonen-die-langdurige-zorg-nodig-hebben](https://www.igj.nl/zorgsectoren/gehandicaptenzorg/publicaties/toetsingskaders/2017/07/01/toetsingskader-voor-zorgaanbieders-waar-mensen-wonen-die-langdurige-zorg-nodig-hebben)

Een inspectierapport geeft de resultaten weer van een uitgevoerde inspectie ter plaatste of naar aanleiding van een onderzoek. Naast rapporten naar aanleiding van een inspectiebezoek vindt u hier ook maatregelen die de inspectie heeft genomen. Zoals verscherpt toezicht, bevel of een aanwijzing.  
De inspectierapporten zijn te vinden via: [https://toezichtdocumenten.igj.nl/](https://toezichtdocumenten.igj.nl/)