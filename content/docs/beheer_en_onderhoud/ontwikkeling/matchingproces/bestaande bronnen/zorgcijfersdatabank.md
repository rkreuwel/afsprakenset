---
title: Zorgcijfersdatabank
weight: 8
---
Zorgcijfersdatabank is een van de websites van Zorginstituut Nederland. Het Zorginstituut ontvangt veel (financiële) data om alle verschillende wettelijke taken goed te kunnen uitvoeren. Onder meer financiële data en declaratiegegevens van verzekerde zorg via zorgverzekeraars en zorgkantoren. Deze datastromen worden geanalyseerd met gebruikmaking van de brede zorginhoudelijke kennis van het gezondheidszorgstelstel waarover het Zorginstituut beschikt. Zo wordt een bijdrage geleverd aan het beter zichtbaar maken van kostenontwikkelingen in de zorg. Daarnaast bieden deze analyses aanknopingspunten voor het doorontwikkelen en evalueren van beleid. Zorgcijfersdatabank biedt een overzicht van deze gegevens en van de rapporten en onderzoeken die hieruit voortkomen.

**Data over zorgkosten**  
Het grootste deel van de gegevens die op Zorgcijfersdatabank gepresenteerd worden zijn data over de zorgkosten in Nederland. Het gaat hierbij om cijfers over de Zorgverzekeringswet (Zvw) en de Wet langdurige zorg (Wlz). Zie voor meer informatie:  [https://www.zorgcijfersdatabank.nl/toelichting/hoe-werkt-de-site](https://www.zorgcijfersdatabank.nl/toelichting/hoe-werkt-de-site).

**Onderzoeken**
Het Zorginstituut doet ook zelf onderzoek met de financiële datastromen. Deze onderzoeken zijn soms onderdeel van een groter rapport of staan op zichzelf als losse rapportages. Zorgcijfersdatabank biedt een overzicht van al deze onderzoeken zodat ze makkelijk op één plek terug te vinden zijn. Deze onderzoeken bieden aanknopingspunten en ondersteuning voor onder andere beleidsmakers in de zorg, die dankzij de gepresenteerde inzichten veel beter weten wat er precies in het zorgstelsel gebeurt en welke kosten daarmee gemoeid zijn. Zie voor de onderzoeken: [https://www.zorgcijfersdatabank.nl/toelichting/onderzoek](https://www.zorgcijfersdatabank.nl/toelichting/onderzoek).

**Wachtlijstinformatie**
In opdracht van het ministerie van VWS publiceert Zorginstituut Nederland maandelijks gedetailleerde overzichten van het aantal mensen dat wacht op langdurige zorg. Correcte en transparante wachtlijstinformatie is een bron van (beleids)informatie. Ook is de wachtlijstinformatie een prestatie-indicator voor zorgkantoren en andere partijen in de zorg en ondersteuning. Het Zorginstituut maakt zich samen met het werkveld sterk voor de openbare publicatie van actuele en juiste wachtlijstinformatie over de langdurige zorg. Zie voor meer informatie: [https://www.zorgcijfersdatabank.nl/toelichting/wachtlijstinformatie/wachtlijstinformatie-wlz](https://www.zorgcijfersdatabank.nl/toelichting/wachtlijstinformatie/wachtlijstinformatie-wlz).

**Verblijfsduur langdurige zorg**
Elk jaar doet het Zorginstituut analyses naar de gemiddelde verblijfsduur in de langdurige zorg. Vanuit het ministerie van VWS en de veldpartijen uit de verpleeghuiszorg bestaat er behoefte om periodiek meer inzicht te hebben in de ontwikkelingen in de verblijfsduur. Het Zorginstituut heeft een wettelijke taak op het gebied van vraag en aanbod in de Wlz en kan daarom voorzien in de behoefte op basis van analyses op declaratiegegevens die het ontvangt van de zorgkantoren. Zie voor meer informatie: [https://www.zorgcijfersdatabank.nl/toelichting/verblijfsduur-wlz](https://www.zorgcijfersdatabank.nl/toelichting/verblijfsduur-wlz).
