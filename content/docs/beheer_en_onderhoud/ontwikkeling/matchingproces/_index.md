---
title: Matchingsproces
weight: 3
---
![Het matchen van informatiebehoefte en geregistreerde gegevens](matchingproces.png)

Het matchingsproces is een samenhangend proces waarbij een zestal stappen wordt doorlopen om op gestructureerde wijze van een informatiebehoefte van een afnemer te komen tot informatievragen, concepten, hun definities en de benodigde gegevenselementen in de modelgegevensset. Door de samenhang tussen informatiebehoefte, -vragen, concepten, definities en gegevenselementen verloopt het matchingsproces niet per definitie lineair. Inzichten over beschikbare concepten (stap 4) kunnen bijvoorbeeld leiden tot een herformulering van de informatiebehoefte (stap 1 en 2) of tot een voorstel voor een uitbreiding of aanpassing (stap 5).

Het matchingsproces kent de volgende stappen:

1. **Voorbereiding - De afnemer formuleert zijn informatiebehoefte**. Dit doet de afnemer aan de hand van de volgende vragen:
   * **a.** Wat is het doel van de gegevensverzameling?
     * **i.** Juridisch: Op basis van welke (al dan niet wettelijke) grondslag vindt de gegevensverzameling plaats? En met welk doel?
     * **ii.** Business case: Wat is de waarde van de gegevensverzameling voor de Afnemer? Heeft de gegevensverzameling waarde voor de Aanbieder en zo ja, op welke manier? Is de cliënt/maatschappij gebaat bij de gegevensverzameling en zo ja, op welke manier? Is de informatiebehoefte eenmalig (dan is geen uitwisselprofiel nodig) of structureel? Wat zijn eventuele alternatieve manieren om aan de informatie te komen?
   * **b.** Welke informatievragen heeft de Afnemer?
2. **De beheerorganisatie en afnemer lopen gezamenlijk de informatiebehoefte na**. De beheerorganisatie en afnemer lopen gezamenlijk de vragen uit de eerste stap na en scherpen waar nodig de antwoorden aan. Deze stap wordt afgerond met een go/no go-moment waarbij beheerorganisatie en afnemer samen op basis van juridische grondslag en business case bepalen of een vervolg van het matchingsproces zin heeft.
3. **De beheerorganisatie ontleedt met de afnemer de informatievragen in de concepten waaruit deze zijn opgebouwd**. Concepten worden, samen met hun definitie, opgenomen in de modelgegevensset. Een mogelijk concept is _“personeel met een tijdelijk contract” dat voor afnemer A bestaat uit 1) een persoon met 2) een arbeidsovereenkomst bij 3) een zorginstelling die 4) van tijdelijke aard is_.
4. **De beheerorganisatie zoekt met de afnemer per concept naar een bestaande omschrijving.** Een concept is bij voorkeur al in de modelgegevensset KIK-V opgenomen. Is dat niet het geval, dan wordt een nieuw concept voor de modelgegevensset voorgesteld. De omschrijving dient bij voorkeur voort te komen uit een bron die 1) inhoudelijk aansluit, 2) aansluit op wet- en regelgeving en 3) breed gedragen wordt. Voor personeelssamenstelling is bijvoorbeeld de CAO VVT een voor de hand liggende bron: deze bestaat binnen het domein en is door de ketenpartijen overeengekomen. Een concept kan uit één of meerdere elementen bestaan. Deze moeten allen benoemd worden. Het concept _“arbeidsovereenkomst”_ omvat bijvoorbeeld de elementen _“ingangsdatum”, “einddatum”, “aantal uur”, etc_.
5. **Indien de benodigde gegevenselementen niet beschikbaar zijn, dan kan de beheerorganisatie met de afnemer uitbreiding van de modelgegevensset overwegen**. De relevante afwegingen daarvoor staan hieronder beschreven. In alle gevallen geldt dat indien de beheerorganisatie en de afnemer over willen gaan tot een aanpassing van de modelgegevensset, dit loopt via een wijzigingsverzoek aan de afsprakenset, zoals beschreven bij [Ontwikkeling afsprakenset](/content/docs/beheer_en_onderhoud/ontwikkeling/afsprakenset). Deze stap eindigt met een go/no go-moment waarbij beheerorganisatie en afnemer samen bepalen in hoeverre de (al dan niet aangepaste) modelgegevensset kan voorzien in de informatiebehoefte.

## Afwegingskader uitbreiding modelgegevensset

Bij stap 5 wordt het Afwegingskader uitbreiding modelgegevensset gehanteerd. Het afwegingskader beschrijft de toetsvragen die moeten worden gesteld indien een afnemer gegevenselementen wenst die niet beschikbaar zijn binnen de bestaande modelgegevensset. Onder aan de streep moet het afwegingskader een antwoord geven op de vraag: “Weegt de veronderstelde waarde van het gegevenselement voor de afnemer op tegen de benodigde inspanning bij de aanbieder?”

De onderbouwing van de keuze om de modelgegevensset uit te breiden kan worden opgenomen in de Toelichting op het uitwisselprofiel.

![Afwegingskader uitbreiding modelgegevensset](afwegingskader_uitbreiding_modelgegevensset.png)

Naast het perspectief van afnemers en aanbieders dient ook het bredere KIK-V-perspectief te worden meegewogen bij uitbreiding van de modelgegevensset. De volgende meer generieke afwegingen bieden daarbij houvast:

* In hoeverre past de gewenste uitbreiding binnen de principes en grondslagen van de afsprakenset?
* In hoeverre draagt de gewenste uitbreiding bij aan de verdere standaardisatie van de gegevensuitwisseling?
* In hoeverre draagt de gewenste uitbreiding bij aan de relevantie en continuïteit van de gegevensset over de jaren heen (is het te koppelen aan een structurele behoefte of door meer partijen gedeelde behoefte)?
* In hoeverre sluit de gewenste uitbreiding aan bij de releasekalender en past deze binnen de aankomende release van de afsprakenset?
* In hoeverre is het concept te positioneren als ‘aanvullende gegevens zorgaanbieder’ en kan deze als zodanig onderdeel zijn van de afspraken hierover en de registratie daarvan door de zorgaanbieder?
* Een zorgaanbieder kan niet gevraagd worden om aanvullende persoonsgegevens te registreren als hier geen wettelijke grondslag voor is.

Op basis van de antwoorden op de vragen in het afwegingskader wordt een advies opgesteld met betrekking tot de voorgestelde actie ten aanzien van de modelgegevensset. Dit advies wordt ingebracht ter besluitvorming bij de Ketenraad KIK-V via het betreffende wijzigingsvoorstel. De weging van de verschillende afwegingen uit bovenstaand kader is vooralsnog niet gestandaardiseerd, omdat de verwachting is dat de weging per gewenst gegevenselement anders uit kan vallen. In het advies wordt daarom de afweging situationeel nader onderbouwd.

De beheerorganisatie evalueert in samenspraak met de deelnemers na elke release van de afsprakenset of het afwegingskader nog voldoet.

In het doorlopen van het afwegingskader kan gebruik worden gemaakt van de informatie op de pagina over [bestaande (openbare) bronnen](/content/docs/beheer_en_onderhoud/ontwikkeling/matchingproces/bestaande%20bronnen).