---
title: Onvoldoende anonimisatie
weight: 2
---
## Procedure onvoldoende onomkeerbaar geanonimiseerde gegevensset

Procedure onvoldoende onomkeerbaar geanonimiseerde gegevensset gaat over de meldpiicht: van de afnemer wanneer ontvangen gegevens onvoldoende onomkeerbaar zijn; en van de aanbieder wanneer onomkeerbare anonimisering van aan te leveren gegevens niet mpgelijk is.

### Meldplicht afnemer

Afnemers zijn verplicht melding te maken van een vanuit de aanbieder ontvangen gegevensset die onvoldoende onomkeerbaar geanonimiseerd is. Hiervoor gelden de volgende afspraken:

- De afnemer meldt het voorval aan de aanbieder aan de hand van minimaal de volgende informatie:
  - De van toepassing zijnde informatie-uitvraag (identifier en naam uitwisselprofiel);
  - De wijze waarop er vanuit de optiek van afnemer sprake is van onvoldoende onomkeerbare anonimisering.
- De aanbieder bepaalt in hoeverre er sprake is van een datalek en of zij melding van het voorval moet doen bij de Autoriteit Persoonsgegevens;
- De afnemer meldt het voorval aan de beheerorganisatie. Hierbij hanteert afnemer dezelfde informatie als richting de aanbieder, inclusief een eerste eigen probleemanalyse (is er sprake van een structureel probleem in het uitwisselprofiel of heeft de aanbieder een fout gemaakt?). De gegevens over de betreffende aanbieder zijn hierbij niet relevant;
- De afnemer verwijdert de gegevensset en maakt afspraken met de aanbieder over het aanleveren van een nieuwe gegevensset. Deze afspraken zijn mede afhankelijk van de vervolgstappen van de afnemer met de beheerorganisatie.

### Meldplicht aanbieder

Andersom geldt ook dat aanbieders verplicht zijn melding te maken van onmogelijkheden om op basis van het uitwisselprofiel tot onomkeerbare anonimisering van de gegevensset te komen. Hiervoor gelden de volgende afspraken:

- De aanbieder meldt de geconstateerde onmogelijkheid aan de afnemer. Mocht de aanbieder geen gehoor krijgen bij de afnemer, dan kan de aanbieder de geconstateerde onmogelijkheid direct melden bij de beheerorganisatie;
- De aanbieder wisselt (het deel van) de onvoldoende onomkeerbaar geanonimiseerde gegevensset niet uit met de afnemer totdat een oplossing is gevonden voor de geconstateerde onmogelijkheden;
- De afnemer meldt de geconstateerde onmogelijkheden van de aanbieder aan de beheerorganisatie, zodat het uitwisselprofiel eventueel kan worden aangepast of verduidelijkt.

### Vervolgstappen afnemer en beheerorganisatie

De beheerorganisatie bepaalt, in afstemming met de afnemer, of er in het uitwisselprofiel sprake is van een structureel probleem bij het onomkeerbaar anonimiseren. Indien het geval, dan gelden de volgende afspraken:

- In overleg met het Tactisch overleg KIK-V schort de beheerorganisatie het uitwisselprofiel tijdelijk op. Het Tactisch overleg bepaalt in hoeverre ook de Ketenraad KIK-V moet worden geïnformeerd.
- De afnemer informeert de aanbieders over de tijdelijke opschorting en de consequenties voor aanlevering van gegevens. Indien bekend, informeert de afnemer aanbieders daarbij ook over de voorziene doorlooptijd van de opschorting;
- De afnemer en beheerorganisatie lossen gezamenlijk het structurele probleem aan het uitwisselprofiel op. Indien nodig vindt hierbij toetsing plaats bij aanbieders;
- De beheerorganisatie laat de wijzigingen aan het uitwisselprofiel vaststellen;
- De beheerorganisatie publiceert de nieuwe versie van het uitwisselprofiel;
- De afnemer informeert de aanbieders schriftelijk over de nieuwe versie van het uitwisselprofiel en de verwachtingen bij de aanlevering van gegevens;
  
Mocht uit de probleemanalyse blijken dat er geen sprake is van een structureel probleem, dan lossen afnemer en aanbieder het geconstateerde voorval onderling op. Waar nodig kan de beheerorganisatie altijd het uitwisselprofiel tekstueel nog verduidelijken.

### Escalatie

In het geval dat de beheerorganisatie en de afnemer het bij de probleemanalyse niet eens kunnen worden over de aard van het probleem, dan wordt het probleem geëscaleerd naar het Tactisch overleg KIK-V en ultimo naar de Ketenraad KIK-V. De beheerorganisatie kan hiervoor een (spoed)vergadering inlassen.

Mocht een aanbieder zich verder onvoldoende gehoord voelen bij de afnemer en beheerorganisatie, dan kan zij escaleren via de branchevertegenwoordiging in het Tactisch overleg KIK-V en ultimo naar de Ketenraad KIK-V.