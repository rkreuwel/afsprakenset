---
title: Wet langdurige zorg (Wlz)
weight: 7
---
Geldend van 01-01-2022 t/m heden

**Relevante artikelen**

_Artikel 4.2.1_
1. De Wlz-uitvoerder heeft een zorgplicht, die inhoudt dat:

    a. hij de bij hem ingeschreven verzekerde informatie verschaft over de leveringsvormen, bedoeld in hoofdstuk 3, paragraaf 3, en deze verzekerde, indien hij in aanmerking kan komen voor meerdere leveringsvormen, in de gelegenheid stelt voor zorg met verblijf in een instelling, voor een volledig pakket thuis of voor een modulair pakket thuis te kiezen of hem wijst op de mogelijkheid om bij het zorgkantoor een persoonsgebonden budget aan te vragen,

    b. indien de verzekerde zorg in natura zal worden verstrekt:

    1°. hij ervoor zorgt dat de zorg waarop de verzekerde aangewezen is binnen redelijke termijn en op redelijke afstand van waar deze wenst te gaan wonen dan wel bij hem thuis, wordt geleverd,

    2°. hij de verzekerde de keuze laat uit alle geschikte, gecontracteerde zorgaanbieders die deze verzekerde de zorg op redelijke termijn kunnen verlenen, of

    3°. hij de verzekerde desgewenst bemiddelt naar geschikte, gecontracteerde zorgaanbieders,

    c. hij ervoor zorgt dat voor de verzekerde cliëntondersteuning beschikbaar is waarop de verzekerde, al dan niet met behulp van zijn vertegenwoordiger of mantelzorger, een beroep kan doen,

    d. hij ervoor zorgt dat voor een verzekerde waarop de Wet zorg en dwang psychogeriatrische en verstandelijk gehandicapte cliënten toepassing vindt of voor diens vertegenwoordiger een cliëntenvertrouwenspersoon als bedoeld in hoofdstuk 4A van die wet beschikbaar is.

2. Het zorgkantoor heeft een zorgplicht, die inhoudt dat:

    a. hij de verzekerden die wonen in de regio waarvoor hij als zorgkantoor is aangewezen, desgevraagd informatie verschaft over de voorwaarden waaronder zij in aanmerking kunnen komen voor een persoonsgebonden budget,

    b. hij, indien hij met toepassing van artikel 3.3.3 een persoonsgebonden budget heeft verleend, ervoor zorgt dat het budget binnen redelijke termijn beschikbaar wordt gesteld.

3. [Red: Dit lid is nog niet in werking getreden.]

4. De voordracht voor een krachtens het derde lid vast te stellen algemene maatregel van bestuur wordt niet eerder gedaan dan vier weken nadat het ontwerp aan beide kamers der Staten-Generaal is overgelegd.

_Artikel 4.2.2_
1. Ter uitvoering van zijn zorgplicht sluit een Wlz-uitvoerder schriftelijke overeenkomsten met zorgaanbieders die zorg kunnen verlenen die ingevolge artikel 3.1.1 verzekerd is.
2. De overeenkomsten bevatten ten minste bepalingen over:

    a. de ingangsdatum van de overeenkomst, de duur van de overeenkomst en de mogelijkheden voor tussentijdse beëindiging van de overeenkomst;

    b. de aard, de kwaliteit, de doelmatigheid en de omvang van de te verlenen zorg;

    c. de prijs van de te verlenen zorg;

    d. de wijze waarop de verzekerden van informatie worden voorzien;

    e. de wijze waarop bij de zorgverlening mantelzorgers en vrijwilligers betrokken kunnen worden;

    f. de controle op de naleving van de overeenkomst, waaronder begrepen de controle op de te verlenen dan wel verleende zorg en op de juistheid van de daarvoor in rekening gebrachte bedragen;

    g. de administratieve voorwaarden die partijen bij de uitvoering van de overeenkomst in acht zullen nemen.

3. De duur van een overeenkomst bedraagt maximaal vijf jaar.

4. De Wlz-uitvoerder draagt er zorg voor dat in het aanbod van gecontracteerde zorgaanbieders redelijkerwijs rekening wordt gehouden met de godsdienstige gezindheid, de levensovertuiging, de culturele achtergrond en de seksuele gerichtheid van de bij hem ingeschreven verzekerden.

5. Indien na beëindiging van een overeenkomst voor een bepaalde vorm van zorg door een Wlz-uitvoerder geen aansluitende overeenkomst voor die vorm van zorg met dezelfde zorgaanbieder tot stand komt, behoudt de verzekerde, zolang die zorg noodzakelijk en verantwoord is, jegens de Wlz-uitvoerder recht op ononderbroken voortzetting van die vorm van zorg, te verlenen door dezelfde zorgaanbieder, indien die zorg is aangevangen voor de datum waarop de overeenkomst met die zorgaanbieder voor de desbetreffende vorm van zorg is beëindigd.

6. Gedurende de tijdelijke voortzetting van de zorg, bedoeld in het vijfde lid, gelden tussen de Wlz-uitvoerder en de zorgaanbieder de voorwaarden van de overeenkomst waaronder de zorg aan de in het vijfde lid bedoelde verzekerde is aangevangen, behoudens voor zover bij ministeriële regeling anders wordt bepaald.

7. [Red: Dit lid is nog niet in werking getreden.]

8. De voordracht voor een krachtens het zevende lid vast te stellen algemene maatregel van bestuur wordt niet eerder gedaan dan vier weken nadat het ontwerp aan beide kamers der Staten-Generaal is overgelegd.

_Artikel 9.2.1_

Bij of krachtens algemene maatregel van bestuur kunnen in het belang van de zorgverlening, de bekostiging daarvan en de afstemming op andere wettelijke voorzieningen, regels worden gesteld over de kosteloze verstrekking van informatie van beleidsmatige en beheersmatige aard:

  a. door zorgaanbieders aan Wlz-uitvoerders, de zorgautoriteit en Onze Minister,

  b. door Wlz-uitvoerders aan de zorgautoriteit en Onze Minister,

  c. door het CIZ aan Onze Minister.

2. De bij of krachtens algemene maatregel van bestuur te stellen regels als bedoeld in het eerste lid, hebben geen betrekking op persoonsgegevens als bedoeld in de Algemene verordening gegevensbescherming en worden niet gesteld dan nadat met door zorgaanbieders of de Wlz-uitvoerders voorgedragen koepelorganisaties, overleg is gevoerd over de inhoud van de in het eerste lid bedoelde gegevens en standaardisering van de wijze waarop de gegevens worden verstrekt.

3. Onverminderd het bepaalde in het tweede lid, worden voor de verstrekking van informatie door het CIZ aan Onze Minister, bedoeld in het eerste lid, onderdeel c, door het CIZ persoonsgegevens, waaronder gegevens over de gezondheid, als bedoeld in artikel 4, onderdeel 15, van de Algemene verordening gegevensbescherming verwerkt.

4. Bij of krachtens algemene maatregel van bestuur kan worden bepaald dat het overleg, bedoeld in het tweede lid, ook plaatsvindt met andere organisaties en instanties dan genoemd in het tweede lid, en kan worden bepaald dat het eerste en tweede lid ook van toepassing is ten aanzien van die organisaties en instanties.

_Artikel 10.4.1_
1. De ambtenaren van de Inspectie gezondheidszorg en jeugd zijn belast met het toezicht op de naleving door zorgaanbieders van de verplichtingen die voor hen uit het bepaalde bij of krachtens [hoofdstuk 8](https://wetten.overheid.nl/BWBR0035917/2019-11-27#Hoofdstuk8) voortvloeien.
2. De in het eerste lid bedoelde ambtenaren zijn, voor zover dat voor de vervulling van hun taak noodzakelijk is, bevoegd tot inzage van de dossiers van verzekerden. In afwijking van [artikel 5:20, tweede lid, van de Algemene wet bestuursrecht](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&artikel=5:20&g=2019-12-05&z=2019-12-05), dienen ook zorgverleners die uit hoofde van hun beroep tot geheimhouding van de dossiers verplicht zijn, de ambtenaren, bedoeld in de eerste volzin, inzage te geven in de daar bedoelde dossiers. In dat geval zijn de betrokken ambtenaren verplicht tot geheimhouding van de dossiers.

**Toepassing voor de afspraken**
* Het zorgkantoor heeft een zorgplicht. Om deze zorgplicht uit te voeren sluit het zorgkantoor overeenkomsten met V&V zorgaanbieders. 

* Met name onderdelen b, f en g over wat de overeenkomsten minstens moeten bevatten, zijn relevant in het kader van uitwisseling van gegevens tussen zorgkantoren (WLZ-uitvoerders) en zorgaanbieders in het kader van KIK-V.

* Artikelen 4.2.1 t/m 4.2.6 geven aan wat minimaal aan cliëntkeuzeinformatie beschikbaar moet zijn, namelijk hetgeen waarmee rekening gehouden wordt in het aanbod aan (toekomstige) cliënten (zie bijvoorbeeld 4.2.1 of 4.2.2 lid 4).

* Artikel 9.2.1 biedt de grondslag voor de uitwisseling van gegevens tussen zorgaanbieders en (de minister van) VWS ten behoeve van beleidsinformatie.

* Artikel 10.4.1 stelt dat de IGJ toezicht moet houden op de invulling van de randvoorwaarden door zorgaanbieders zoals gesteld in hoofdstuk 8 van de Wlz. De randvoorwaarden in hoofdstuk 8 gaan over de afspraken die de zorgaanbieder maakt met de cliënt over goede zorg. Daarbij mogen ze dossiers van verzekerden inzien.