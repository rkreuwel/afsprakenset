---
title: De Nederlandse Grondwet, Artikel 68
weight: 4
---
**Relevante artikelen**
*Artikel 68 Inlichtingenplicht ministers, staatssecretarissen; interpellatie*
De ministers en de staatssecretarissen geven de kamers elk afzonderlijk en in verenigde vergadering mondeling of schriftelijk de door een of meer leden verlangde inlichtingen waarvan het verstrekken niet in strijd is met het belang van de staat.

**Toepassing voor de afspraken**
In *Artikel 68* is geregeld dat inlichtingen aan de kamers mogen worden verstrekt zolang deze niet in strijd zijn met het belang van de staat. Dit is een belangrijke grondslag van het Ministerie van VWS.