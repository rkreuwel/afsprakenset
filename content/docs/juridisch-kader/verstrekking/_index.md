---
title: Verstrekking
weight: 4
---
De sectie Verstrekking bestaat uit:

* [Algemene wet bestuursrecht (AWB)](/content/docs/juridisch-kader/verstrekking/awb.md)
* [Artikel 7 Burgerlijk Wetboek Boek 7 (BW-B7-Art7)](/content/docs/juridisch-kader/verstrekking/bw-b7-art7.md)
* [Doorleveren van informatie](/content/docs/juridisch-kader/verstrekking/doorleveren.md)
* [De Nederlandse Grondwet, Artikel 68](/content/docs/juridisch-kader/verstrekking/grondwet.md)
* [Mededingingswet](/content/docs/juridisch-kader/verstrekking/mw.md)
* [Wet kwaliteit, klachten en geschillen zorg (WKKGZ)](/content/docs/juridisch-kader/verstrekking/wkkgz.md)
* [Wet langdurige zorg (Wlz)](/content/docs/juridisch-kader/verstrekking/wlz.md)
* [Regeling openbare jaarverantwoording WMG](/content/docs/juridisch-kader/verstrekking/wmg.md)
* [Wet marktordening gezondheidszorg (WMGZ)](/content/docs/juridisch-kader/verstrekking/wmgz.md)
* [Wet toetreding zorgaanbieders (Wtza)](/content/docs/juridisch-kader/verstrekking/wtza.md)
* [Zorgverzekeringswet (Zvw)](/content/docs/juridisch-kader/verstrekking/zvw.md)