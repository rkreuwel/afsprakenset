---
title: Regeling openbare jaarverantwoording WMG
weight: 8
---
**Relevante artikelen**
_Artikel 13_
1. De openbaarmaking geschiedt langs elektronische weg door deponering van de volledig in de Nederlandse taal en euro gestelde jaarverantwoording bij het CIBG via het platform DigiMV.
2. Bij het deponeren van de financiële verantwoording en de daarbij te voegen stukken, mag de handtekening door de accountant, interne toezichthouder, eigenaar, bestuurder of vennoot achterwege blijven, onder vermelding van de voor- en achternaam van diegene waarvan de ondertekening bij deponering achterwege is gelaten.
3. De vastgestelde jaarverantwoording wordt openbaar gemaakt met inachtneming van hetgeen omtrent de financiële toestand tussen 31 december van het boekjaar en de datum van vaststelling is gebleken.
4. Indien blijkt dat de openbaar gemaakte jaarverantwoording in ernstige mate tekortschiet, dan meldt de zorgaanbieder dit onverwijld bij het CIBG via het elektronische platform DigiMV overeenkomstig [bijlage 5](https://wetten.overheid.nl/BWBR0045649/2022-01-01/#Bijlage5) bij deze regeling.

**Toepassing voor de afspraken**
* Met artikel 13.1 wordt geregeld dat het CIBG het agentschap is waar de gegevens moeten worden aangeleverd, verwerkt en weer openbaar beschikbaar worden gesteld. Via het platform DigiMV.
