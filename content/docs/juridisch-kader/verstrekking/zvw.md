---
title: Zorgverzekeringswet (Zvw)
weight: 11
---
Geldend van 01-01-2022 t/m heden

**Relevante artikelen**
_Artikel 88_
1. Een ieder verstrekt op verzoek aan de zorgverzekeraars, het Zorginstituut, de zorgautoriteit, Onze Minister, de rijksbelastingdienst, het Uitvoeringsinstituut werknemersverzekeringen, de Sociale verzekeringsbank, het college van burgemeester en wethouders, het CAK, of aan een daartoe door of vanwege een van deze zorgverzekeraars of instanties aangewezen persoon kosteloos alle inlichtingen en gegevens, waaronder persoonsgegevens als bedoeld in de Algemene verordening gegevensbescherming, die noodzakelijk zijn voor de uitvoering van de zorgverzekeringen of van deze wet of van artikel 11g van de Wet kwaliteit, klachten en geschillen zorg.
2. De in het eerste lid bedoelde gegevens en inlichtingen worden op verzoek verstrekt in schriftelijke vorm of in een andere vorm die redelijkerwijs kan worden verlangd, binnen een termijn die schriftelijk wordt gesteld bij het in het eerste lid bedoelde verzoek.
3. Een ieder geeft op verzoek van een rechtspersoon als bedoeld in het eerste lid, inzage in alle bescheiden en andere gegevensdragers, stelt deze op verzoek ter beschikking voor het nemen van afschrift en verleent de terzake verlangde medewerking, voor zover dit noodzakelijk is voor de uitvoering van deze wet door de desbetreffende zorgverzekeraars of instanties.
4. Bij ministeriële regeling kunnen nadere regels worden gesteld met betrekking tot het eerste, tweede of derde lid.

_Artikel 89_
1\. De in [artikel 88, eerste lid](https://wetten.overheid.nl/BWBR0018450/2019-07-01#Hoofdstuk7_Artikel88), bedoelde zorgverzekeraars en instanties, alsmede de Wlz-uitvoerders, zijn bevoegd uit eigen beweging en verplicht op verzoek binnen een bij dat verzoek genoemde termijn, uit de onder hun verantwoordelijkheid gevoerde administratie, aan elkaar, aan een daartoe door of vanwege hen aangewezen persoon of aan een door Onze Minister aangewezen persoon, kosteloos, de gegevens, waaronder persoonsgegevens als bedoeld in de Algemene verordening gegevensbescherming, te verstrekken die noodzakelijk zijn voor de uitvoering van de zorgverzekeringen of van deze wet, van artikel 11g van de Wet kwaliteit, klachten en geschillen zorg, of voor de onderlinge afstemming van op grond van de zorgverzekering verzekerde zorg en zorg die is verzekerd op grond van de [Wet langdurige zorg](https://wetten.overheid.nl/jci1.3:c:BWBR0035917&g=2019-10-14&z=2019-10-14).

6\. De verstrekking aan het Zorginstituut op grond van artikel 88, eerste lid of het eerste lid, betreft gegevens waaronder gegevens over gezondheid als bedoeld in artikel 4, onderdeel 15 van de Algemene verordening gegevensbescherming, die noodzakelijk zijn voor:  
a. de berekening van de aan een zorgverzekeraar toekomende bijdragen als bedoeld in artikelen 32 tot en met 34;  
b. de uitvoering van de aan het Zorginstituut in de artikelen 64 of 66 opgedragen taken; of  
c. de uitvoering van zijn in artikel 11g van de Wet kwaliteit, klachten en geschillen zorg, opgedragen taken.

7\. Op de aan Onze Minister of het Zorginstituut te verstrekken persoonsgegevens is pseudonimisering als bedoeld in artikel 4, onderdeel 5 van de Algemene verordening gegevensbescherming, toegepast die vervolgens onafgebroken is gecontinueerd.

**Toepassing voor de afspraken**
* Artikel 88 regelt dat een ieder onder andere aan zorgverzekeraars, het Zorginstituut, de NZa en Onze Minister alle inlichtingen en gegevens verstrekt die nodig zijn voor het uitvoeren van (de wettelijke taken in) de zorgverzekeringswet en de Wkkgz.
* Artikel 88 lid 2 geeft alle ruimte om afspraken te maken over de vorm waarin gegevens worden aangeleverd.
* Artikel 88 lid 3 regelt ook inzage in alle bescheiden en andere gegevensdragers aan partijen die dat nodig hebben om hun taak uit deze wet in te vullen.
* Artikel 88 lid 4: In de Regeling zorgverzekering zijn nadere afspraken opgenomen over de verwerking van persoonsgegevens voor bepaalde doeleinden. Veelal heeft dit betrekking op de gegevensuitwisseling tussen zorgaanbieders en -verzekeraars. Voor het uitwisselen van kwaliteitsdata lijken er geen nadere regels te zijn opgesteld in de regeling.
* Artikel 89 lid 1 geeft alle vrijheid aan de in artikel 88 lid 1 genoemde organisaties om onderling gegevens uit te wisselen voor de uitvoering van de wet. Dat betekent bijvoorbeeld dat de NZa door hen verzamelde gegevens mag doorleveren aan het Zorginstituut indien die gegevens nodig zijn voor de uitvoering van de taken ingevolge de Zvw en de Wkkgz van het Zorginstituut. Ook hierbij geldt trouwens dat het Zorginstituut alleen gepseudonimiseerd persoonsgegevens mag verwerken (artikel 89 lid 7).
