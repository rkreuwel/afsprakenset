---
title: Wet kwaliteit, klachten en geschillen zorg (WKKGZ)
weight: 1
---
# [Wet kwaliteit, klachten en geschillen zorg (WKKGZ)](https://wetten.overheid.nl/BWBR0037173/2020-01-01)

Geldend van 01-01-2020 t/m heden

**Relevante artikelen**
_Artikel 7_
1. De zorgaanbieder draagt zorg voor systematische bewaking, beheersing en verbetering van de kwaliteit van de zorg.
2. De verplichting van het eerste lid houdt, de aard en omvang van de zorgverlening in aanmerking genomen, in:
   * a. het op systematische wijze verzamelen en registreren van gegevens betreffende de kwaliteit van de zorg op zodanige wijze dat de gegevens voor eenieder vergelijkbaar zijn met gegevens van andere zorgaanbieders van dezelfde categorie;
   * b. het aan de hand van de gegevens, bedoeld in onderdeel a, op systematische wijze toetsen of de wijze van uitvoering van artikel 3 leidt tot goede zorg;
   * c. het op basis van de uitkomst van de toetsing, bedoeld in onderdeel b, zo nodig veranderen van de wijze waarop artikel 3 wordt uitgevoerd.

_Artikel 11_
1. De zorgaanbieder doet bij de inspectie onverwijld melding van:
   1. iedere calamiteit die bij de zorgverlening heeft plaatsgevonden;
   2. geweld in de zorgrelatie;
   3. de opzegging, ontbinding of niet-voortzetting van een overeenkomst als bedoeld in [artikel 4, eerste lid, onderdeel b](https://wetten.overheid.nl/BWBR0037173/2022-01-01#Hoofdstuk2_Paragraaf1_Artikel4), met een zorgverlener op grond van zijn oordeel dat de zorgverlener ernstig is tekort geschoten in zijn functioneren.
2. De zorgaanbieder en de zorgverleners die zorg verlenen aan zijn cliënten, verstrekken bij en naar aanleiding van een melding als bedoeld in het eerste lid aan de ingevolge deze wet met toezicht belaste ambtenaar de gegevens, daaronder begrepen persoonsgegevens, gegevens over gezondheid als bedoeld in artikel 4, onderdeel 15 van de Algemene verordening gegevensbescherming en andere bijzondere categorieën van persoonsgegevens en persoonsgegevens van strafrechtelijke aard als bedoeld [paragraaf 3.1](https://wetten.overheid.nl/jci1.3:c:BWBR0040940&paragraaf=3.1&g=2022-04-11&z=2022-04-11) onderscheidenlijk [paragraaf 3.2 van de Uitvoeringswet Algemene verordening gegevensbescherming](https://wetten.overheid.nl/jci1.3:c:BWBR0040940&paragraaf=3.2&g=2022-04-11&z=2022-04-11), die voor het onderzoeken van de melding noodzakelijk zijn.

**Toepassing voor de afspraken**
* Artikel 7 van de Wet kwaliteit, klachten en geschillen zorg (Wkkgz) verplicht zorgaanbieders om aan systematische bewaking, beheersing en verbetering van de zorg te doen, onder meer door het op systematische wijze verzamelen en registreren van kwaliteitsgegevens op een wijze die ze vergelijkbaar maakt met die van andere zorgaanbieders in dezelfde categorie. Het artikel biedt een grondslag voor de opname van persoonsgegevens in de aanbiedergegevensset en de daaropvolgende aggregatie. Dit omdat het artikel spreekt van “systematisch” en “vergelijkbaar”. Daar vloeit een transformatie als die naar de aanbiedergegevensset conform het gegevenswoordenboek eigenlijk al uit voort.
* Artikel 11 van de Wet kwaliteit, klachten en geschillen zorg (Wkkgz) verplicht zorgaanbieders om melding te doen bij de IGJ op het moment dat er sprake is van een calamiteit bij zorgverlening, geweld in de zorgrelatie of opzegging, ontbinding of niet-voortzetting van een overeenkomst met een zorgverlener op het moment dat de zorgverlener ernstig tekort is geschoten in zijn functioneren.