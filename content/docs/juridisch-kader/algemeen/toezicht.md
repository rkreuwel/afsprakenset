---
title: Toezicht en controle op de naleving
weight: 3
---
**Relevante artikelen**
_In de zorg zijn verschillende instanties die wettelijk toezicht houden. Dit toezicht op de uitvoering van geldende wet- en regelgeving blijft onverminderd van kracht. Via de KIK-V afspraken wordt slechts aanvullend toezicht gedefinieerd op de nakoming van afspraken in het stelsel._

De instanties die wettelijk toezicht houden, zijn:

* [Autoriteit Persoonsgegevens (AP)](https://www.autoriteitpersoonsgegevens.nl/) - De Autoriteit Persoonsgegevens houdt toezicht op de naleving van de wettelijke regels voor bescherming van persoonsgegevens en adviseert over nieuwe regelgeving;
* [Inspectie Gezondheidszorg en Jeugd (IGJ)](https://www.igj.nl) - De Inspectie Gezondheidszorg en Jeugd is onafhankelijk toezichthouder in de Nederlandse gezondheidszorg. Door toezicht, handhaving en opsporing van strafbare feiten bewaken en bevorderen zij de veiligheid en kwaliteit van zorg;
* [Nederlandse Zorgautoriteit (NZa)](https://www.nza.nl) - De Nederlandse Zorgautoriteit zet zich in voor goede en betaalbare zorg die beschikbaar is als je die nodig hebt. Vanuit dat perspectief maakt de NZa regels en houdt zij toezicht op zorgaanbieders en zorgverzekeraars;
* [Working Party](http://www.consilium.europa.eu/en/council-eu/preparatory-bodies/working-party-telecommunications-information-society) op grond van artikel 29 van de Europese richtlijn (alle toezichthouders op persoonsgegevens in Europa gezamenlijk, in Nederland AP) - De Working Party geeft ‘Opinions’ hoe de wet geïnterpreteerd moet worden. Zoals de interpretatie van voorwaarden voor anonimiseren, certificeren en PIA’s.

**Toepassing voor de afspraken**

In de afsprakenset KIK-V wordt controle op de naleving van de verplichtingen van de KIK-V afspraken geregeld. Er zal vanuit KIK-V niet worden toegezien op de uitvoering van wet- en regelgeving door de deelnemers aan de KIK-V afsprakenset. Dit is de verantwoordelijkheid van de genoemde toezichthouders. De KIK-V afspraken betreffen aanvullende (privaatrechtelijke?) afspraken op wet- en regelgeving.

Overtredingen van de wet- en regelgeving kunnen wel gevolgen hebben voor de positie van de Deelnemer in het stelsel.