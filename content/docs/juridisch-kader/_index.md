---
title: Juridisch kader
weight: 4
---
Het juridisch kader beschrijft relevante wet- en regelgeving voor de afsprakenset KIK-V en brengt de belangrijkste kaders voor de oplossing in beeld. Om zo min mogelijk betekenis te verliezen, zijn in de tabel de relevante artikelen uit wet- en regelgeving overgenomen in de derde kolom. In de vierde kolom is vervolgens opgenomen hoe de artikelen worden geïnterpreteerd en op welke manier ze van toepassing zijn voor de oplossing.

De wet- en regelgeving is in de eerste kolom geordend in categorieën die overeenkomen met de verwerkingen binnen KIK-V. Zie voor een toelichting op deze verwerkingen de kolom ‘Toepassing voor de afspraken' bij de Algemene Verordening Gegevensbescherming, tevens in onderstaande tabel. Voor de wet- en regelgeving die niet specifiek betrekking heeft op een van de verwerkingen is een categorie ‘Algemeen’ toegevoegd.

Rondom specifieke doelen of wettelijke taken van partijen kunnen aanvullende regels zijn over welke (persoons)gegevens verwerkt mogen worden. Het voert te ver om dit voor elk doel of elk soort gegevens toe te lichten in het kader. Op het moment dat de gegevensuitwisseling wordt uitgewerkt in een uitwisselprofiel, kan worden nagegaan of er nadere restricties/inkadering zijn voor het uitwisselen van gegevens.

Het juridisch kader pretendeert niet volledig te zijn en het blijft de verantwoordelijkheid van deelnemende partijen om te voldoen aan wet- en regelgeving. Er kunnen daarmee geen rechten aan het juridisch kader worden ontleend.