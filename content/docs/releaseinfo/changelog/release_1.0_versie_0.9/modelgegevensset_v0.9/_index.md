---
title: Modelgegevensset v0.9
weight: 1
---
De gegevensset v0.5 heeft richting v0.9 een transitie doorgemaakt en kent daardoor een andere presentatie. Waar v0.5 bestond uit een set van losse elementen, bestaat v0.9 uit een ontologie waarin onderlinge relaties tussen elementen worden weergegeven.
  
Zo bevat v0.9 concepten met eigenschappen. De namen van de elementen zijn hiermee ook gewijzigd. De wijzigingen zijn met name functioneel van aard en zullen hieronder worden toegelicht. 

## Concepten beperkt tot uitvraag ODB
In v0.9 zijn enkel de concepten opgenomen die benodigd zijn voor het beantwoorden van de indicatoren ODB. Hierdoor zijn er een aantal concepten uit de gegevensset die hier niet voor nodig zijn verplaatst naar de backlog. Op de backlog wordt hierover verdere toelichting gegeven. Bij uitbreiding van het model wordt bepaald of deze concepten aan een volgende versie worden toegevoegd.
  
De volgende elementen zijn verplaatst naar de backlog:
- organisatie (AGB code)
- organisatie.locatie (AGB code)
- cliënt.prestatie
- cliënt.prestatie.tarief
- werknemer.arbeidsovereenkomst.kosten
- inhuurovereenkomst.kosten
- inhuurovereenkomst.uren
- onderaannemer
- onderaannemer.inzet
- onderaannemer.inzet.persoon
- onderaannemer.inzet.uren
- onderaannemer.kosten
- vacature.begindatum
- vacature.einddatum
- vrijheidsbeperkendeInterventie (in afwachting van besluitvorming indicatoren 2021)
- vrijheidsbeperkendeInterventie.soortInterventie (in afwachting van besluitvorming indicatoren 2021)
- vrijheidsbeperkendeInterventie.aanvangEpisode (in afwachting van besluitvorming indicatoren 2021)
- vrijheidsbeperkendeInterventie.eindeEpisode (in afwachting van besluitvorming indicatoren 2021)
  
## Overeenkomsten
In de gegevensset v0.9 is ‘werkovereenkomst’ het paraplubegrip. Alle soorten overeenkomsten vallen onder dit concept. Alle personen met een werkovereenkomst zijn medewerker. Het soort overeenkomst bepaalt uiteindelijk wat voor soort rol een persoon heeft (e.g. een persoon met een stage-overeenkomst heeft de rol stagiair). Voor deze rollen zijn dus geen aparte concepten opgenomen in de gegevensset. Tot slot bepaalt het soort overeenkomst ook tot wat voor groep een persoon behoort (personeel in loondienst / personeel niet in loondienst).

![Relaties overeenkomsten](relaties-overeenkomsten.png)

Dit vervangt in v0.9 de volgende elementen:

- werknemer
- werknemer.arbeidsovereenkomst
- werknemer.arbeidsovereenkomst.ingangsdatum
- werknemer.arbeidsovereenkomst.einddatum
- werknemer.arbeidsovereenkomst.type
- werknemer.arbeidsovereenkomst.leerling
- vrijwilliger
- stagiair
- inhuurovereenkomst

## Start- en einddatum
In v0.9 zijn start- en einddatum niet meer opgenomen als losse elementen, maar zijn dit eigenschappen van bepaalde concepten. De volgende concepten kennen de eigenschappen start- en einddatum:

1. Overeenkomst: elke soort overeenkomst kan een start- en einddatum hebben. Dit maakt de gegevensset v0.9 flexibeler dan v0.5, waar enkel de ingangs- en einddatum van een arbeidsovereenkomst als elementen waren opgenomen.
2. Gewerkte periode: het aantal uren dat gewerkt is in een bepaalde periode kan worden bepaald door een periode te definiëren. Een periode wordt aangeduid middels een start- en einddatum.
3. Ziekteperiode
4. Functie: een functie kan wijzigen zonder dat de arbeidsovereenkomst wijzigt. Om die reden is ook voor het concept functie een start- en einddatum toegevoegd.
5. Kwalificatieniveau
  
Dit vervangt in v0.9 de volgende elementen:

- werknemer.arbeidsovereenkomst.ingangsdatum
- werknemer.arbeidsovereenkomst.einddatum
- werknemer.datumEersteZiektedag
- werknemer.hersteldatum
- zorgverlener.kwalificatieniveau.ingangsdatum

## Zorgverlener
In v0.5 kon door zorgaanbieders worden aangegeven of een medewerker een zorgverlener is middels ja / nee. In v0.9 wordt het begrip zorgverlener gesplitst in twee concepten:

1. Concept functie: een zorgaanbieder registreert functies. Hierbij wordt uitgegaan van een niet-uitsluitende lijst aan functies. Dit is naar eigen invulling door de zorgaanbieder. De zorgaanbieder bepaalt welke functies zijn te classificeren als zorgverlener.
2. Concept zorgverlener functie: zorgverlener is een rol die een persoon wel of niet heeft. Iemand heeft de rol zorgverlener als: iemand een geldig contract heeft en de functie op dat contract door de zorgaanbieder geclassificeerd is als zorgverlener.
  
Dit vervangt in v0.9 het volgende element:
- zorgverlener

## Gewerkte uren
In v0.5 werd het concept gewerkte uren omschreven als het aantal verloonde uren (inclusief verzuim en verlof). In versie 0.9 is nu gekozen om te werken met de daadwerkelijk gewerkte of ingezette uren (zonder verzuim en verlof). Dit om een zuiver beeld te geven van de uren die daadwerkelijk zijn ingezet.
  
Dit vervangt in v0.9 de volgende elementen:

- urengewerkt
- urengewerkt.aantal
- urengewerkt.verlof
- urengewerkt.periode

## Ziektepercentage
In v0.5 was enkel een element arbeidsongeschiktheidsfactor opgenomen om het ziekteverzuimpercentage te berekenen. Echter, ook ziektepercentage als concept is nodig om de indicator volledig te kunnen berekenen. Een medewerker hoeft namelijk niet altijd voor 100% ziek te zijn, dit kan ook een ander percentage zijn.

## Datum en onderwerp bijeenkomst
Alle verschillende bijeenkomsten (bespreking in 0.5, zoals casuïstiekbespreking of bespreking voedselvoorkeuren) zijn geen aparte gegevenselementen meer. In plaats daarvan is er een concept bijeenkomst opgenomen en die kan een relatie hebben met verschillende onderwerpen (zoals  medicatiefout of voedselvoorkeuren). Aan de hand van die relatie kan bepaald worden waar de bijeenkomst (bespreking) over ging. Door te selecteren op onderwerp kunnen de juiste bijeenkomsten verzameld worden behorend bij een indicator. Van die bijeenkomsten met dat onderwerp kan dan de datum bepaald worden. In v0.9 kunnen concepten de eigenschap datum hebben. Hierdoor is datum geen apart gegevenselement meer (zoals in v0.5), maar een eigenschap van een concept. Zo heeft dus het concept bijeenkomst een datum als eigenschap.
  
Dit vervangt in v0.9 de volgende elementen:

- casuïstiekbespreking.datum
- medicatiefout.datumBespreking
- voedselvoorkeuren.datumBespreking
