---
title: Release 2.0 versie 0.91
weight: 2
---
Afsprakenset:
* Op de algemene pagina over de afsprakenset is het nieuwe onderwerp Gegevensuitwisseling met datastations toegevoegd aan de toelichting op de afsprakenset.
* Referenties naar de komst van de afspraken over gegevensuitwisseling zijn weggehaald, evenals opmerkingen over de scope van de afsprakenset release 1.0 (die beperkt was tot het kwaliteitskader verpleeghuiszorg)

Grondslagen:
* Pagina ‘Afbakening eerste conceptversie afsprakenset’ is verwijderd uit de afsprakenset en opgenomen in het archief. Door de verdieping van de afsprakenset en uitwisselprofielen in het programma KIK-V2 is de beschrijving van deze eerste afbakening voor release 1.0 en 1.1 van de afsprakenset niet meer van toepassing.
* Randvoorwaarde R6 over de tijdigheid van een eerste versie van de afsprakenset is verwijderd. Deze randvoorwaarde is niet meer van toepassing voor de fase van doorontwikkeling waarin de afsprakenset KIK-V zich inmiddels bevindt.

Juridisch kader:
* Wijzigingen in de Wkkgz zijn opgenomen, evenals het effect daarvan op de Zvw.
* Beschrijving van de toepassing van artikelen in de Wlz voor zorgkantoren als grondslag voor de gegevensuitwisseling met zorgaanbieders in het kader van de contractering is toegevoegd.
* De Wzd als nieuwe wetgeving en de relevante artikelen voor het juridisch kader afsprakenset KIK-V is toegevoegd.
* De Wtza die in de plaats is gekomen van de Wtzi en de relevante artikelen voor het juridisch kader afsprakenset KIK-V is toegevoegd.
* Artikel 13 Regeling openbare jaarverantwoording WMG is toegevoegd, aangezien hiernaar verwezen wordt in de Wtza en ter vervanging van soortgelijke bepalingen in de Wtzi.

Model gegevensset:
* Toevoegingen in de model gegevensset (ontologie) ten behoeve van de ondersteuning van de nieuwe versie van het uitwisselprofiel ODB kwaliteitskader (o.a. ontologie basisveiligheid) en de toekomstige uitwisselprofielen voor IGJ, Zorgkantoren, NZA en VWS.
* Aanpassing benodigd voor mogelijk zorgbrede toepassing van de ontologie (breder dan de verpleeghuiszorg).
* Een toelichting toegevoegd over de Ontologie en wat dat is.

Model uitwisselprofiel:
* Toevoegingen aan een aantal bestaande onderwerpen in het model uitwisselprofiel op basis van de ervaringen met het gebruik ervan.
* Enkele nieuwe onderwerpen toegevoegd op basis van ervaringen uit de toepassing van het model uitwisselprofiel evenals uit de ontwikkeling van de afspraken over gegevensuitwisseling met datastations, pilot KIK-starter en de ontwikkeling van datastations as a service.
* Onderwerp bij algemeen over de verantwoordelijkheid van de afnemer na ontvangen van de antwoorden geschrapt. De beschrijving van deze verantwoordelijkheid komt al terug in de afsprakenset over privacy- en beveiliging en in het model uitwisselprofiel op het onderwerp terugkoppeling.
* Enkele toevoegingen die betrekking hebben op de afspraken over het eventueel niet kunnen aanleveren door zorgaanbieders.
* Toevoeging van de onderwerpen frequentie van de vraag, doorlooptijd voor beantwoording, termijnen voor de historie en looptijd van het uitwisselprofiel, evenals welke sleutelgegevens nodig zijn voor het identificeren van de aanbieder.

Uitwisselkalender:
* Aangepast met informatie uit de in ontwikkeling zijnde uitwisselprofielen en de informatie uit niet meer relevante uitwisselingen (zonder uitwisselprofiel) zijn verwijderd.

Beheerafspraken:
* De pagina ‘Beheerafspraken’ is aangepast en meer in lijn gebracht met de manier waarop de beheersprocessen nu worden ingericht door de Beheerorganisatie KIK-V.
  * De titel “Beheerafspraken” is herzien naar “Beheer en onderhoud”, aangezien dat de lading van dit hoofdstuk beter dekt.
  * Een uitgebreidere inleiding is toegevoegd met daarin de omschrijving van de ITIL methode (die aan de basis ligt van beheer en onderhoud KIK-V) en uitlegt dat dit hoofdstuk m.n. gaat over het releaseproces van KIK-V producten als onderdeel van die methode.
  * In het releaseproces zijn twee stappen hernoemd:
    * de fase “ontwikkelen” is veranderd in “concretiseren” → concretiseren klinkt meer inclusief. het is niet alleen het ontwikkelen, maar ook de planning, afstemming en oplevering rondom de ontwikkeling.
    * de fase “gebruiken en evalueren” in “evalueren” → het gebruik van de producten zit als uitgangspunt verwerkt in de fases implementeren en evalueren
  * Het releaseprocesfiguur (_figuur 1_) is herzien naar aanleiding van de gewijzigde fasenamen.
  * De releaseprocesfiguur inclusief weergave van doorlooptijdenverschil (_figuur 2_) is herzien naar aanleiding van de gewijzigde fasenamen.
  * De doorlooptijdverschillen van de fasen zijn aangescherpt in een ”Aandachtspunt-blok”
  * Per fase is er een sectie doelen en relevante vragen toegevoegd ter verduidelijking van die fase.
  * De inhoud van de fasen van het releaseproces zijn geherformuleerd en veralgemeniseerd naar KIK-V producten breed (i.p.v. specifieke benoeming uitwisselprofielen en Afsprakenset). Inhoudelijk hebben er in de omschrijvingen van de fasen geen grote veranderingen plaatsgevonden.


* Op pagina Ontwikkeling afsprakenset is het proces onderverdeeld in 3 nieuwe kopjes voor de leesbaarheid.
* De pagina ‘Metadata Uitwisselprofiel’ is tijdelijk verwijderd uit de afsprakenset. Deze specificaties worden momenteel nog niet toegepast door de beheerorganisatie en speelt met name een rol in de verdere uitwerking van de afspraken over gegevensuitwisseling via datastations. In de onderwerpen voor doorontwikkeling wordt dit meegenomen bij het onderwerp datacatalogi.
* De pagina ‘Betrokkenheid aanbieders bij ontwikkeling’ is samengevat en aangepast naar de actuele manier van samenwerken met Actiz en zorgaanbieders. De verdere invulling is meer algemeen gehouden, zodat dit in de toekomst steeds flexibel afgestemd kan worden op de actuele praktijk.
* Op de pagina ‘Matchingsproces’ zijn op basis van ervaring met de uitvoering daarvan de stappen 4 en 5 samengevoegd waarmee in totaal 5 stappen overblijven. Over de gehele pagina wordt tevens alleen gesproken over ‘concepten’ (en bijbehorende elementen) in de modelgegevensset in plaats van de aparte term gegevenselementen ook toe te passen. Deze beschrijving komt beter overeen met de huidige opzet van de model gegevensset (ontologie).
* Aan de pagina ‘Bestaande openbare bronnen’ zijn de volgende openbare bronnen en een beschrijving per bron toegevoegd: Zorgkaart Nederland (PFN); LRZA (CIBG); Openbaar DataBestand (ZIN); Zorgcijfersdatabank; Inspectierapporten (IGJ); Databank (CIZ)

Afspraken uitwisseling datastations:

De afspraken gegevensuitwisseling datastations vormen een nieuw, generiek onderdeel van de Afsprakenset KIK-V en bevat vooralsnog alleen een overkoepelende inleiding met daarin onder meer het te ondersteunen functionele proces, de gehanteerde principes en een beschrijving van de (scope)keuzes die zijn gemaakt omwille van een samenhangende technische implementatie.

De afspraken zijn, in aanvulling op consultatie, bedoeld ter oriëntatie van partijen en verdere praktijkbeproeving.
