---
title: Release 2.1 versie 0.9
weight: 1
---
## Juridisch kader
* Toegevoegd grondslag in de WLZ voor uitvragen door (minister van) VWS ten behoeve van het uitwisselprofiel VWS beleidsinformatie

* Toegevoegd grondslag in de WLZ voor uitvragen door zorgkantoren naar cliëntenformatie

## Model uitwisselprofiel
* Toegevoegd dat aangegeven moet worden of er gebruik gemaakt kan worden van een verdeelsleutel en welke

## Uitwisselkalender
Uitgebreid met de gegevens die gebruikt worden in de nieuwe uitwisselprofielen.

## Modelgegevensset
* Release 2.1 van de ontologie ter ondersteuning van de nieuwe uitwisselprofielen. Met name de toevoeging van financiële concepten.

## Beheerafspraken
* Aan de pagina ‘Bestaande openbare bronnen’ is de openbare bron regio-analyses NZA toegevoegd.
