---
title: Release 1.0 versie 0.91
weight: 5
---
- Wijzigingen naar aanleiding van laatste input uit Tactisch Overleg december 2020
- Aanpassing figuren [opbouw afsprakenset](/content/docs/opbouw/) (uitwisselcontexten als apart onderwerp)
- Aanpassing [betrokkenheid aanbieders bij ontwikkeling](/content/docs/beheer_en_onderhoud/ontwikkeling/betrokkenheid_aanbieders/)
- Aanpassing titel pagina Procesafspraken stuurgroep kwaliteitskader voor uitwisselprofiel ODB met ‘voor uitwisselprofiel ODB’