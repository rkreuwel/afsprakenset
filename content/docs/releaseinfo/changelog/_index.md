---
title: Changelog
weight: 2
---
De sectie bevat de Changelogs vpor de volgende versies:

* [Release 1.0 versie 0.9](/content/docs/releaseinfo/changelog/release_1.0_versie_0.9/)
* [Release 1.0 versie 0.91](/content/docs/releaseinfo/changelog/release_1.0_versie_0.91.md)
* [Release 1.0 versie 0.92](/content/docs/releaseinfo/changelog/release_1.0_versie_0.92.md)
* [Release 1.1 versie 0.9](/content/docs/releaseinfo/changelog/release_1.1_versie_0.9.md)
* [Release 2.0 versie 0.91](/content/docs/releaseinfo/changelog/release_2.0_versie_0.91.md)
* [Release 2.1 versie 0.9](/content/docs/releaseinfo/changelog/release_2.1_versie_0.9.md)