---
title: Release 1.0 versie 0.92
weight: 4
---
Wijzigingen naar aanleiding van laatste input uit Tactisch Overleg schriftelijke ronde 1e week januari 2021

- Oude changelog tot versie 0.9 is verplaatst naar het archief en niet meer opgenomen in de afsprakenset.
- Bij het [Modeluitwisselprofiel](/content/docs/modeluitwisselprofiel/) is nu in [principe 8](/content/docs/modeluitwisselprofiel/principes.md) een verwijzing opgenomen naar het interoperabiliteitsmodel voor gegevensuitwisseling.