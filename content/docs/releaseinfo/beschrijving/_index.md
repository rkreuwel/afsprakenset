---
title: Beschrijving
weight: 1
---
| Release | 2.2 |
|:-|:-|
| Versie | 2.2 versie 0.9|
| Doel | Release 2.2 betreft de vijfde publicatie van de afsprakenset en bevat wijzigingen benodigd bij of als basis voor de implementatie van nieuwe uitwisselprofielen |
| Doelgroep | (A) Ketenpartijen KIK-V; (B) Zorgaanbieders verpleeghuiszorg; (C) andere geïnteresseerden in de KIK-V Afsprakenset|
| Totstandkoming | De ontwikkeling van release 2.2 is uitgevoerd onder leiding van het project Afspraken binnen het programma KIK-V, in samenwerking met de belanghebbende partijen. Release 2.2 wordt vastgesteld door de Ketenraad KIK-V op basis van versie 0.9. |
| Inwerkingtreding | Na besluit Ketenraad |
| Operationeel toepassingsgebied | De afsprakenset release 2.2 dient als basis voor implementatie van nieuwe uitwisselprofielen die in de zomer van 2023 en daarna worden vastgesteld |
| Status | Ter vaststelling door Ketenraad KIK-V |
| Functionele scope | Release 2.2 omvat hetgeen gepubliceerd in release 2.1, aangevuld met: 
(A) uitbreidingen van de model gegevensset voor de ondersteuning van nieuwe uitwisselprofielen, met name op de onderwerpen financiële gegevens, capaciteit, cliënt(keuze)informatie en organisatie informatie;
(B) afspraken over toepassen van ‘aanvullende gegevens zorgaanbieders’, (C) informatie over de zorgaanbieder die gebruikt wordt in verschillende uitwisselprofielen;
(D) uitbreiding van de uitwisselkalender met de gegevens die gebruikt worden in de nieuwe uitwisselprofielen
verwerking van aanbevelingen uit de risico analyse naar risico’s voor privacy- en beveiliging bij het hanteren van de specificaties voor de gegevensuitwisseling met datastations |
| Licentie | [Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal (CC BY-ND 4.0)](https://creativecommons.org/licenses/by-nd/4.0/).