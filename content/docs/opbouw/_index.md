---
title: Opbouw afsprakenset
weight: 1
---
De afsprakenset KIK-V is een samenhangende set van afspraken en is als volgt opgebouwd:

![Kaders Uitwisselprofielen KIK-V](Kaders_uitwisselprofielen_KIK-V_v3.png)

De afsprakenset kent de volgende onderdelen:

* (Niet in afbeelding) [Releaseinfo](/content/docs/releaseinfo/) beschrijft de belangrijkste kenmerken van de release van de set en wat de verschillen zijn ten opzichte van eerdere versies;
* [Juridisch kader](/content/docs/juridisch-kader/) beschrijft als context de voor de set relevante wet- en regelgeving en geeft daarmee inzicht in de beschikbare speelruimte binnen de afsprakenset;
* [Privacy en informatiebeveiliging deelnemers](/content/docs/privacy_en_informatiebeveilinging/) beschrijft als context de voor de set relevante verplichtingen die rusten op deelnemers op het gebied van privacy en informatiebeveiliging. Op de pagina zijn in aanvulling op de bestaande verplichtingen een aantal voor KIK-V relevante aanbevelingen opgenomen;
* [Grondslagen](/content/docs/grondslagen/) beschrijft de basisbeginselen van de afsprakenset en vormt daarmee het fundament voor de rest van de afspraken. De overige producten bouwen voort op de grondslagen;
* [Modelgegevensset](/content/docs/modelgegevensset/) standaardiseert de gegevensuitwisseling op informatie/semantisch niveau. De modelgegevensset beschrijft de gegevens die door aanbieders gebruikt kunnen worden voor de gegevensuitwisseling beschreven in de uitwisselprofielen KIK-V;
* [Uitwisselkalender](/content/docs/uitwisselkalender/) beschrijft in samenhang de aanlever- en terugkoppelmomenten binnen KIK-V en is daarmee een instrument dat inzicht geeft in de timing van de gegevensuitwisseling;
* [Modeluitwisselprofiel](/content/docs/modeluitwisselprofiel/) geeft een (functionele) beschrijving van de onderdelen van een uitwisselprofiel. In een uitwisselprofiel worden de afspraken opgenomen over de gegevensuitwisseling ten behoeve van een specifieke afnemer en zijn doel;
* [Sturingsstructuur](/content/docs/sturingsstructuur/) beschrijft op welke wijze de sturing op de afspraken wordt vormgegeven en de plek van het beheer daarin;
* [Beheer en onderhoud](/content/docs/beheer_en_onderhoud/) beschrijven ten slotte hoe de afspraken tot stand komen, worden onderhouden en hoe doorontwikkeling van de set plaatsvindt;
* Uitwisselprofielen beschrijft de specifieke contexten waarbinnen gegevens worden uitgewisseld. Uitwisselprofielen worden omwille van de flexibiliteit los van de afsprakenset vastgesteld en gepubliceerd (zie [Beheer en onderhoud](/content/docs/beheer_en_onderhoud/ontwikkeling/uitwisselprofielen/)).